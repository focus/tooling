"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import pyomo.environ as pyomo
from pyomo.opt import SolverStatus, TerminationCondition
import matplotlib.pyplot as plot
import os

def pareto_analysis(prosumer, model, strategy_names):
    strategy_name_1 = strategy_names[0]
    strategy_name_2 = strategy_names[1]
    solver = pyomo.SolverFactory("gurobi")
    solver.options['MIPGap'] = 0.01
    solver.options['Presolve'] = 2
    solver.options['TimeLimit'] = 200
    payoff = [[0.0, 0.0], [0.0, 0.0]]
    f1 = getattr(model, 'f' + strategy_name_1)
    O1 = getattr(model, 'O' + strategy_name_1)
    f2 = getattr(model, 'f' + strategy_name_2)
    O2 = getattr(model, 'O' + strategy_name_2)
    O2.deactivate()
    solver.solve(model)
    payoff[0][0] = pyomo.value(f1)
    model.cp11 = pyomo.Constraint(expr = f1 == payoff[0][0])
    O1.deactivate()
    O2.activate()
    solver.solve(model)
    payoff[0][1] = pyomo.value(f2)
    model.del_component(model.cp11)
    solver.solve(model)
    payoff[1][1] = pyomo.value(f2)
    model.cp22 = pyomo.Constraint(expr = f2 == payoff[1][1])
    O2.deactivate()
    O1.activate()
    solver.solve(model)
    payoff[1][0] = pyomo.value(f1)
    r2 = payoff[1][1] - payoff[0][1]
    g2 = 4
    i2 = 0
    number_of_solutions = 0
    model.s2 = pyomo.Var(bounds = (0, None))
    model.e2 = pyomo.Param(mutable = True)
    model.del_component(model.cp22)
    if O2.sense == pyomo.maximize:
        factor = -1
    else:
        factor = 1
    model.cp2 = pyomo.Constraint(expr = f2 + factor * model.s2 == model.e2)
    O1.deactivate()
    model.O = pyomo.Objective(expr = f1 + 0.001 * (model.s2 / r2), sense = O1.sense)
    f1_list = []
    f2_list = []
    pareto_rsl = dict()
    while i2 <= g2:
        model.e2 = payoff[0][1] + (i2 / g2) * r2
        solver_result = solver.solve(model)
        if solver_result.solver.status == SolverStatus.ok and solver_result.solver.termination_condition == TerminationCondition.optimal:
            f1_list.append(pyomo.value(f1))
            f2_list.append(pyomo.value(f2))
            pareto_rsl[number_of_solutions] = (pyomo.value(model.x1), pyomo.value(model.x2))
            number_of_solutions += 1
            i2 += 1
        else:
            i2 = g2 + 1
    if not os.path.exists('output_files/'):
        os.makedirs('output_files/')
    if not os.path.exists('output_files/' + prosumer.name + '/'):
        os.makedirs('output_files/' + prosumer.name + '/')
    fig, ax = plot.subplots(figsize = (16, 9))
    ax.plot(f1_list, f2_list, 'o-.', linewidth = 0.5)
    plot.title('Pareto-Front')
    plot.grid(True)
    plot.xlabel(strategy_name_1)
    plot.ylabel(strategy_name_2)
    plot.savefig('output_files/' + prosumer.name + '/pareto_front.png')