"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
"""
This tool use the TEK project of IWU to calculate the annual energy demand of
the building and use the degree day method to generate the demand profile.
"""

import os
import datetime
from warnings import warn
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# ==============================================================================
#                     Non-residential Building List
# ==============================================================================

building_typ_list = ["Verwaltungsgebäude",
                     "Büro und Dienstleistungsgebäude",
                     "Hochschule und Forschung",
                     "Gesundheitswesen",
                     "Bildungseinrichtungen",
                     "Kultureinrichtungen",
                     "Sporteinrichtungen",
                     "Beherbergen und Verpflegen",
                     "Gewerbliche und industrielle",
                     "Verkaufsstätten",
                     "Technikgebäude",
                     "Wohngebäude",
                     "Wohngebäude (MFH)"]
energy_typ_list = ["sehr hoch", "hoch", "mittel", "gering", "sehr gering"]

# ==============================================================================
#                       Path for inputs and outputs
# ==============================================================================

# Automatic Data Imports
base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
input_profile_path = os.path.join(base_path, "tek_data", "GHD_profile.xlsx")
input_dhw_path = os.path.join(base_path, "tek_data", "DHW_profile.xlsx")
input_energy_path = os.path.join(base_path, "tek_data",
                                 "TEK_Teilenergiekennwerte.xlsx")
input_zone_path = os.path.join(base_path, "tek_data", "GHD_Zonierung.xlsx")
input_tabula_path = os.path.join(base_path, "tek_data", "TABULA_data.xlsx")
output_path = os.path.join(base_path, "data", "tek_data", "output_heat_profile")


def gen_heat_profile(building_typ,
                     area,
                     temperature_profile,
                     year=2021,
                     energy_typ="mittel",
                     plot=False,
                     save_plot=False):
    """
    total_degree_day: K*h
    annual_value: kW*h, jährlicher Gesamt Heizwärmebedarf
    Using degree day method to calculate the heat profil, the set temperature depending
    on room type and heating start at the temperature of 15 degree.
    :return:
    """
    # Analysis thermal zones in building
    new_zone_df = analysis_bld_zone(building_typ, area)

    # Calculate demand in each zone and degree day method
    demand_df = pd.read_excel(input_energy_path, sheet_name=energy_typ)
    profile_df = pd.read_excel(input_profile_path, sheet_name='DIN V 18599')
    total_heat_profile = []
    total_heat_demand = 0
    for row in range(len(new_zone_df)):
        zone = new_zone_df.loc[row, 'DIN_Zone']  # Zone name in DIN
        hour_status = op_time_status(year, zone)
        zone_area = new_zone_df.loc[row, 'new_area']
        zone_heat_demand = calc_zone_demand(demand_df, 'heat', zone, zone_area)
        zone_heat_profile = degree_day(zone, zone_heat_demand, profile_df,
                                       temperature_profile, hour_status)
        total_heat_profile = np.sum([total_heat_profile, zone_heat_profile],
                                    axis=0)
        total_heat_demand += zone_heat_demand

    if plot:
        plot_profile(total_heat_profile, save_plot)

    return total_heat_profile


def gen_hot_water_profile(building_typ, area, year=2021, energy_typ="mittel"):
    # new_zone_df = analysis_bld_zone(building_typ, area)
    # for row in range(len(new_zone_df)):
    #     zone = new_zone_df.loc[row, 'DIN_Zone']

    bld_hot_water_demand = calc_bld_demand(building_typ, area, 'hot_water',
                                           energy_typ)
    if building_typ == "Wohngebäude" or building_typ == "Wohngebäude (MFH)":
        hot_water_heating_demand_df = pd.read_excel(input_dhw_path,
                                                    sheet_name='DHW',
                                                    header=None,
                                                    usecols=[1],
                                                    skiprows=1)
        hot_water_heating_demand_df.columns = ['Wärmebedarf für '
                                               'Trinkwassererwärmung (kWh)']
        # The data is for 300 liter hot water demand and 60 grade Celsius supply
        # hot water with 12 grade Celsius inlet water.
        hot_water_heating_demand_df['Aktueller Wärmebedarf für ' \
                                    'Trinkwassererwärmung (kWh)'] = \
            hot_water_heating_demand_df['Wärmebedarf für ' \
                                        'Trinkwassererwärmung (kWh)'].map(
                lambda x: x / (4180 * 300 * (60 - 12) / 3600 / 1000 * 365) *
                          bld_hot_water_demand)

        hot_water_heating_demand_array = np.array(
            hot_water_heating_demand_df['Aktueller Wärmebedarf für '
                                        'Trinkwassererwärmung (kWh)'])

    elif building_typ in building_typ_list and building_typ != "Wohngebäude" \
            or building_typ != "Wohngebäude (MFH)":
        bld_occupancy = calc_bld_occupancy(building_typ, area, year)
        hot_water_heating_demand_array = bld_hot_water_demand / sum(
            bld_occupancy) * np.array(bld_occupancy)
        # print(hot_water_heating_demand_array)

    return hot_water_heating_demand_array


def op_time_status(year, zone):
    """
    Calculate the operating time for whole year. The weekend and work time
    could be considered in this function. For different thermal zone the
    operating time also varies according to DIN V 18599.
    Args:
        year: int, target year
        zone: str, the name should be same as in the standard DIN V 18599
    Returns:
        List of status for each hour in whole year
    """
    weekday_list = find_weekday(year)
    profile_df = pd.read_excel(input_profile_path, sheet_name='DIN V 18599')
    start_time = profile_df.loc[profile_df['Raumtyp'] == zone][
        'Nutzungszeit_von'].values[0]
    end_time = profile_df.loc[profile_df['Raumtyp'] == zone][
        'Nutzungzeit_bis'].values[0]
    if end_time == 0:
        end_time = 24  # 24:00 is 0:00

    status_list = []
    if profile_df.loc[profile_df['Raumtyp'] == zone]['Nutzungstage'].values[
        0] in [150, 200, 230, 250]:
        # The zone are only used in weekday. Zone such as audience hall
        # with 150 operating days and Zone auch as fabric with 230 operating
        # days are also considered as the other working zone.
        day = 0
        while day < 365:
            day_status = [0] * 24
            if weekday_list[day] in [0, 1, 2, 3, 4]:
                day_status[start_time:end_time] = [1] * (end_time - start_time)
            status_list += day_status
            day += 1
    elif profile_df.loc[profile_df['Raumtyp'] == zone]['Nutzungstage'].values[
        0] == 300:
        # The zone which are only used in weekday and Saturday, like restaurant
        day = 0
        while day < 365:
            day_status = [0] * 24
            if weekday_list[day] in [0, 1, 2, 3, 4, 5]:
                day_status[start_time:end_time] = [1] * (end_time - start_time)
            status_list += day_status
            day += 1
    elif profile_df.loc[profile_df['Raumtyp'] == zone]['Nutzungstage'].values[
        0] == 365:
        # The zone are used everyday, such as bedroom
        day = 0
        while day < 365:
            day_status = [0] * 24
            day_status[start_time:end_time] = [1] * (end_time - start_time)
            status_list += day_status
            day += 1
    else:
        # The zone such as hall are not considered
        warn('The operating days of zone' + zone + 'does not match the DIN V '
                                                   '18599')

    return status_list


def find_weekday(year):
    """
    Create a list of weekdays throughout the year. The holidays are not
    considered in the function.
    Leap years are also considered to have only 365 days to reduce the work.
    Args:
        year: int, target year
    Returns:
        list, status for whole year
    """
    # weekday() == 4 means Friday, the value could be from 0 to 6
    day = datetime.date(year, 1, 1).weekday()
    weekday_list = []

    i = 0
    while i < 365:
        weekday_list.append(day)
        day = (day + 1) % 7
        i += 1

    return weekday_list


def analysis_bld_zone(building_typ, area):
    """Analysis the thermal zones in building, the zone, which is smaller
    than the min_zone_area should be ignored. The min_zone_area is hard coded
    with 2 m², which could be fixed later or not :)"""
    zone_df = pd.read_excel(input_zone_path, sheet_name=building_typ,
                            header=None, usecols=range(5), skiprows=2)
    zone_df.columns = ['Nr', 'Zone', 'Percentage', 'kum_per', 'DIN_Zone']
    # 1st try to calculate the area of each zone
    zone_df['Area'] = zone_df['Percentage'].map(lambda x: x * area)

    # Too small zones should be delete
    min_zone_area = 2
    new_zone_df = zone_df[zone_df['Area'] > min_zone_area]

    # Recalculate percentage
    sum_per = new_zone_df['Percentage'].sum()
    pd.options.mode.chained_assignment = None
    new_zone_df['new_per'] = new_zone_df['Percentage'].map(
        lambda x: x / sum_per)
    new_zone_df['new_area'] = new_zone_df['Area'].map(lambda x: x / sum_per)

    return new_zone_df


def calc_bld_demand(building_typ, area, energy_commodity, energy_typ='mittel'):
    """Calculate the total demand of the building by adding up the demand of
    all thermal zones.
    energy_commodity: the energy carrier, could be 'heat', 'cool', 'hot_water',
    'elec', the name is defined in method calc_zone_demand.
    energy_typ: the building energy typ, which is define by project TEK and
    describe the building energy level. It could be the items in energy_typ_list
    """
    # Analysis thermal zones in building
    new_zone_df = analysis_bld_zone(building_typ, area)

    bld_demand = 0
    demand_df = pd.read_excel(input_energy_path, sheet_name=energy_typ)
    for row in range(len(new_zone_df)):
        zone = new_zone_df.loc[row, 'DIN_Zone']
        zone_area = new_zone_df.loc[row, 'new_area']
        zone_demand = calc_zone_demand(demand_df, energy_commodity, zone,
                                       zone_area)
        bld_demand += zone_demand

    return bld_demand


def calc_zone_demand(demand_df, demand_typ, zone_typ, zone_area):
    """Calculate the annual total heat demand for each thermal zone"""
    total_demand = 0  # Unit kWh

    column_name = ''
    column_name_light = 'Beleuchtung'
    column_name_other = 'Arbeitshilfen'
    if demand_typ == 'heat':
        column_name = 'Heizung'
    elif demand_typ == 'cool':
        column_name = 'Kühlkälte'
    elif demand_typ == 'hot_water':
        column_name = 'Warmwasser'
    elif demand_typ == 'elec':
        pass
    else:
        warn('The demand typ is not allowed!')

    if demand_typ == 'elec':
        zone_demand = demand_df[demand_df['Standard-Nutzungseinheiten'] ==
                                zone_typ][column_name_light].values[0] + \
                      demand_df[demand_df['Standard-Nutzungseinheiten'] ==
                                zone_typ][column_name_other].values[0]
    else:
        zone_demand = demand_df[demand_df['Standard-Nutzungseinheiten'] ==
                                zone_typ][column_name].values[0]
    total_demand += zone_demand * zone_area

    return total_demand


def calc_bld_occupancy(building_typ,
                       area,
                       year):
    """According to standard SIA2024, the occupancy for each hour are given
    for each thermal zone"""
    din_table = pd.read_excel(input_profile_path, sheet_name='DIN V 18599')
    sia_table = pd.read_excel(input_profile_path, sheet_name='SIA2024')
    bld_occupancy = [0] * 8760

    new_zone_df = analysis_bld_zone(building_typ, area)
    # print(new_zone_df)

    for row in range(len(new_zone_df)):
        din_zone = new_zone_df.loc[row, 'DIN_Zone']
        sia_zone = din_table.loc[
            din_table['Raumtyp'] == din_zone, 'Raumtyp_SIA'].values[0]
        hour_status = op_time_status(year, din_zone)
        # print(sia_zone)
        zone_area = new_zone_df.loc[row, 'new_area']
        sia_pers = sia_table.loc[
            sia_table['Raumtyp'] == sia_zone, 'Personenfläche'].values[0]
        # print(sia_pers)
        sia_pers_profile = sia_table.loc[
            sia_table['Raumtyp'] == sia_zone, 'Personenbelegung'].values[0]
        sia_pers_profile = sia_pers_profile.split(', ')
        sia_pers_profile = list(map(float, sia_pers_profile))
        # print(sia_pers_profile)
        if not np.isnan(sia_pers):
            zone_pers = zone_area / sia_pers
            # print(zone_pers)
            zone_pers_profile = zone_pers * np.array(sia_pers_profile)
            zone_occupancy = [0] * 8760

            for day in range(365):
                for hour in range(24):
                    if hour_status[day*24+hour] == 1:
                        zone_occupancy[day*24+hour] = zone_pers_profile[hour]
            # print(zone_pers_profile)
            bld_occupancy += np.array(zone_occupancy)
    # print(bld_occupancy)
    return bld_occupancy


def degree_day(zone_typ, annual_value, profile_df, temperature_profile,
               status_list):
    heat_profile = []
    start_temp = 15  # The limit for heating on or off, could be the same as
    # set temperature? 15 comes from the german
    set_temp_heat = profile_df[profile_df['Raumtyp'] == zone_typ][
        'Raum-Solltemperatur_Heizung '].values[0]

    total_degree_day = 0
    for time_step in range(8760):
        if temperature_profile[time_step] < start_temp and \
                status_list[time_step] == 1 and \
                temperature_profile[time_step] < set_temp_heat:
            total_degree_day += (set_temp_heat - temperature_profile[
                time_step])

    for time_step in range(8760):
        if temperature_profile[time_step] < start_temp and \
                status_list[time_step] == 1 and \
                temperature_profile[time_step] < set_temp_heat:
            heat_profile.append(
                (set_temp_heat - temperature_profile[time_step]) /
                total_degree_day * annual_value)
        else:
            heat_profile.append(0)

    return heat_profile


def plot_profile(heat_profile, save_plot=False):
    plt.figure()
    plt.plot(heat_profile)
    plt.ylabel('Heat Profile')
    plt.xlabel('Hours [h]')
    plt.ylim(ymin=0)
    plt.xlim(xmin=0)
    plt.grid()
    if save_plot:
        plt.savefig(os.path.join(output_path, 'heat_profile_figure.jpg'))
    plt.show()


def calc_residential_demand(bld_type, bld_year, bld_area,
                            method='TABULA Berechnungsverfahren / korrigiert '
                                   'auf Niveau von Verbrauchswerten',
                            scenario='Ist-Zustand'):
    """According to the IWU TABULA calculate the space heating demand and hot
    water demand. The details of the method could be found in the project
    report 'DE_TABULA_TypologyBrochure_IWU.pdf'
    bld_type: building type, could be 'SFH', 'MFH', 'TH', 'AB'
    bld_year: building construction year, used to find the building class
    bld_area: area of the building
    """
    tabula_df = pd.read_excel(input_tabula_path)
    bld = tabula_df[(tabula_df['Gebäudetyp'] == bld_type) &
                    (tabula_df['Baualtersklasse_von'] < bld_year) &
                    (tabula_df['Baualtersklasse_bis'] >= bld_year) &
                    (tabula_df['Berechnungsverfahren'] == method) &
                    (tabula_df['Szenario'] == scenario)]

    heating_demand = bld['Heizung (Wärmeerzeugung)'].values[0] * bld_area
    hot_water_demand = bld['Warmwasser (Wärmeerzeugung)'].values[0] * bld_area

    return heating_demand, hot_water_demand


if __name__ == "__main__":
    input_temp_path = os.path.join(base_path, 'tek_data', 'temperature.csv')
    temperature = pd.read_csv(input_temp_path)['temperature'].values
    # gen_heat_profile("Wohngebäude", 300, temperature, plot=True)
    # print('Space heating demand profile:')
    # print(gen_heat_profile("Wohngebäude (MFH)", 300, temperature))
    print('Hot water demand profile:')
    print(gen_hot_water_profile("Verwaltungsgebäude", 300))

    # calc_bld_occupancy("Verwaltungsgebäude", 300, year=2021)
