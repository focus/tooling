"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
# Based on demandlib by oemof and Repraesentative Lastprofile (VDEW 1999)
# Smoothing factors pkl for household demand calculated with formula from VDEW 1999
# Formula adjusted for accuracy by oemof: https://github.com/oemof/demandlib/blob/master/demandlib/bdew.py

import datetime
import numpy as np
import pandas as pd
import os
import calendar


def add_weekdays2df(time_df, holidays=None, holiday_is_sunday=False):
    r"""Giving back a DataFrame containing weekdays and optionally holidays for
     the given year.
    Parameters
    ----------
    time_df : pandas DataFrame
        DataFrame to which the weekdays should be added
    Optional Parameters
    -------------------
    holidays : array with information for every hour of the year, if holiday or
        not (0: holiday, 1: no holiday)
    holiday_is_sunday : boolean
        If set to True, all holidays (0) will be set to sundays (7).
    Returns
    -------
    pandas.DataFrame : DataFrame with weekdays
    Notes
    -----
    Using Pandas > 0.16
    """
    time_df['weekday'] = time_df.index.weekday + 1
    time_df['date'] = time_df.index.date

    # Set weekday to Holiday (0) for all holidays
    if holidays is not None:
        if isinstance(holidays, dict):
            holidays = list(holidays.keys())
        time_df['weekday'].mask(pd.to_datetime(time_df['date']).isin(
            pd.to_datetime(holidays)), 0, True)

    if holiday_is_sunday:
        time_df.weekday.mask(time_df.weekday == 0, 7, True)

    return time_df


class ElectricalDemand:
    """Generate electrical standardized load profiles based on the BDEW method.
    Attributes
    ----------
    datapath : string
        Path to the csv files containing the load profile data.
    date_time_index : pandas.DateTimeIndex
        Time range for and frequency for the profile.
    Parameters
    ----------
    year : integer
        Year of the demand series.
    Optional Parameters
    -------------------
    seasons : dictionary
        Describing the time ranges for summer, winter and transition periods.
    holidays : dictionary or list
        The keys of the dictionary or the items of the list should be datetime
        objects of the days that are holidays.
    """

    def __init__(self, year, seasons=None, holidays=None):
        if calendar.isleap(year):
            hoy = 8784
        else:
            hoy = 8760
        file_dir = os.path.dirname(os.path.abspath(__file__))
        for j in range(1):
            file_dir = os.path.dirname(file_dir)
        self.datapath = os.path.join(file_dir, 'bdew_data')
        self.date_time_index = pd.date_range(
            datetime.datetime(year, 1, 1, 0), periods=hoy * 4, freq='15Min')
        if seasons is None:
            self.seasons = {
                'summer1': [5, 15, 9, 14],  # summer: 15.05. to 14.09
                'transition1': [3, 21, 5, 14],  # transition1 :21.03. to 14.05
                'transition2': [9, 15, 10, 31],  # transition2 :15.09. to 31.10
                'winter1': [1, 1, 3, 20],  # winter1:  01.01. to 20.03
                'winter2': [11, 1, 12, 31],  # winter2: 01.11. to 31.12
            }
        else:
            self.seasons = seasons
        self.year = year
        self.slp_frame = self.all_load_profiles(self.date_time_index,
                                                holidays=holidays)

    def all_load_profiles(self, time_df, holidays=None):
        slp_types = ['h0', 'g0', 'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'l0',
                     'l1', 'l2']
        new_df = self.create_bdew_load_profiles(time_df, slp_types,
                                                holidays=holidays)

        new_df.drop(['hour', 'weekday'], 1, inplace=True)
        return new_df

    def create_bdew_load_profiles(self, dt_index, slp_types, holidays=None):
        """Calculates the hourly electricity load profile in MWh/h of a region.
        """

        # define file path of slp csv data
        file_path = os.path.join(self.datapath, 'selp_series.csv')

        # Read standard load profile series from csv file
        selp_series = pd.read_csv(file_path)
        tmp_df = selp_series
        # Create an index to merge. The year and month will be ignored only the
        # time index is necessary.
        index = pd.date_range(
            datetime.datetime(2007, 1, 1, 0), periods=2016, freq='15Min')
        tmp_df.set_index(index, inplace=True)

        # Create empty DataFrame to take the results.
        new_df = pd.DataFrame(index=dt_index, columns=slp_types).fillna(0)
        new_df = add_weekdays2df(new_df, holidays=holidays,
                                 holiday_is_sunday=True)

        new_df['hour'] = dt_index.hour + 1
        new_df['minute'] = dt_index.minute
        time_df = new_df[['date', 'hour', 'minute', 'weekday']].copy()
        tmp_df[slp_types] = tmp_df[slp_types].astype(float)

        # Inner join the slps on the time_df to the slp's for a whole year
        tmp_df['hour_of_day'] = tmp_df.index.hour + 1
        tmp_df['minute_of_hour'] = tmp_df.index.minute
        left_cols = ['hour_of_day', 'minute_of_hour', 'weekday']
        right_cols = ['hour', 'minute', 'weekday']
        tmp_df = tmp_df.reset_index()
        tmp_df.pop('index')

        for p in self.seasons.keys():
            a = datetime.datetime(self.year, self.seasons[p][0],
                                  self.seasons[p][1], 0, 0)
            b = datetime.datetime(self.year, self.seasons[p][2],
                                  self.seasons[p][3], 23, 59)

            new_df.update(pd.DataFrame.merge(
                tmp_df[tmp_df['period'] == p[:-1]], time_df[a:b],
                left_on=left_cols, right_on=right_cols, how='inner').sort_values(
                by=["date", "weekday", "hour", "minute"]).set_index(
                time_df[a:b].index).drop(
                ['hour_of_day'], 1))

        new_df.drop('date', axis=1, inplace=True)
        return new_df.div(new_df.sum(axis=0), axis=1)

    def get_profile(self, ann_demand, slp_type,
                    dyn_function_h0: bool = True):
        """ Get the profiles for the given annual demand
        Parameters
        ----------
        :param ann_demand : number
            annual demand for the prosumer
        :param dyn_function_h0: bool, default True
            Uses the dynamization function of the BDEW to smoothen the
            seasonal edges. Functions resolution is daily.
            f(x) = -3.916649251 * 10^-10 * x^4 + 3,2 * 10^-7 * x³ - 7,02
            * 10^-5 * x²+0,0021 * x +1,24
            Adjustment of accuracy: from -3,92 to -3.916649251
        Returns
        -------
        pandas.Series : Selected profile in time steps of 15 minutes
        :param slp_type: Indicates consumer type
        """
        if dyn_function_h0:
            quartersinyear = len(self.slp_frame)
            # vectorization of the smoothing proposed by oemof in order to save computation time (the dynamization
            # function presented above was resolved outside of the project since it is not dependant on any parameters)
            self.slp_frame['h0'] = \
                np.multiply(np.array(self.slp_frame['h0']),
                            np.array(pd.read_pickle(os.path.join(self.datapath, 'smooth.pkl'))[:quartersinyear]))
        return self.slp_frame[slp_type]*ann_demand*4
