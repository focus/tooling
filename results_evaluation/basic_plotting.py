"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import matplotlib.style as style
import pandas as pd
import os
import matplotlib.pyplot as plt
import numpy as np
style.use('fivethirtyeight')


def plt_results(self, *variables, plt_type='standard', dpi=500, name='results_plot'):
    """
    Plot the results as a whole properly.
    Example:
    # plt_results(('H2_E', 'prss_st'), ('elec_cns', 'electroly'), ('fc', 'elec_cns'), ('fc', 'water_tes'),
    # name='hydrogen components', dpi=100)
    """
    file_dir = os.path.dirname(os.path.abspath(__file__))
    for j in range(2):
        file_dir = os.path.dirname(file_dir)

    for rsl in self._rsl:
        if plt_type == 'standard':
            fig, base_plt = plt.subplots(figsize=(16, 9))
            base_plt.plot(self._BaseProsumer_rsl[rsl]['TimeStep'],
                          self._BaseProsumer_rsl[rsl][variables[0]], linewidth=1.5)
            for variable in variables[1:]:
                self._rsl[rsl].plot(ax=base_plt, x='TimeStep', y=variable, linewidth=1.5)
            plt.legend(title='Variable', labels=variables)
            # plt.legend(['PV', 'Grid Injection', 'Battery Charging', 'Autonomous Consumption'], loc=1)
            plt.show()
            plt.savefig(os.path.join(file_dir, 'output_files', name + '_' + str(rsl) + '.png'), dpi=dpi)
        elif plt_type == 'weekday' and len(variables) == 2:
            averages = dict()
            avg_dfs = dict()
            for variable in variables:
                averages[variable] = []
            for d in range(7):
                for variable in variables:
                    averages[variable].append(self._rsl[rsl].loc[self._rsl[rsl]['TimeStep'].dt.weekday == d]
                                              [variable].mean())
            for variable in variables:
                avg_dfs[variable] = pd.DataFrame(np.array(averages[variable]), columns=['Average ' + str(variable)])

            fig, ax1 = plt.subplots(figsize=(16, 9))

            ax1.set_xlabel('Day of the week')
            ax1.set_ylabel(str(variables[0]))
            ax1.plot(avg_dfs[variables[0]], color='tab:blue')
            ax1.tick_params(axis='y')

            ax2 = ax1.twinx()
            ax2.set_ylabel(str(variables[1]))
            ax2.plot(avg_dfs[variables[1]], color='tab:red')
            ax2.tick_params(axis='y')

            fig.legend([str(item) for item in variables], loc=1)
            fig.tight_layout()
            plt.show()
            plt.savefig(os.path.join(file_dir, 'output_files', name + '_' + str(rsl) + '.png'), dpi=dpi)
        elif plt_type == 'hour' and len(variables) == 2:
            averages = dict()
            avg_dfs = dict()
            for variable in variables:
                averages[variable] = []
            for h in range(24):
                for variable in variables:
                    averages[variable].append(
                        self._rsl[rsl].loc[self._rsl[rsl]['TimeStep'].dt.hour == h][variable].mean())

            for variable in variables:
                avg_dfs[variable] = pd.DataFrame(np.array(averages[variable]), columns=['Average ' + str(variable)])

            fig, ax1 = plt.subplots(figsize=(16, 9))

            ax1.set_xlabel('Hour of the day')
            ax1.set_ylabel(str(variables[0]))
            ax1.plot(avg_dfs[variables[0]], color='tab:blue')
            ax1.tick_params(axis='y')

            ax2 = ax1.twinx()
            ax2.set_ylabel(str(variables[1]))
            ax2.plot(avg_dfs[variables[1]], color='tab:red')
            ax2.tick_params(axis='y')

            fig.legend([str(item) for item in variables], loc=1)
            fig.tight_layout()
            plt.show()
            plt.savefig(os.path.join(file_dir, 'output_files', name + '_' + str(rsl) + '.png'), dpi=dpi)
        else:
            print('Please input a valid combination of variables and plot type.')