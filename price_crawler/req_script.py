"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import os
import requests
import re
import warnings
from bs4 import BeautifulSoup
import pandas as pd


'''
Create input xlsx file with headline "URL" and the URLs you want to pull data from listed in the rows below
Define "equipment" with what you want to analyse in the main method!
Output: .csv
'''


def get_source(html):
    """
    The method using package request to get html file from website
    :param html: url for html file
    :return: html file as string
    """
    code = requests.get(html).text
    return code


def get_string(source, df, equipment):
    """
    The method parse the html file and get the price for each product,
    the price data are stored into a pandas dataframe.
    :param source: html file as string
    :param df: dataframe for previous data, could be empty dataframe
    :param equipment: type of equipment for collecting price data
    :return: dataframe with price information
    """
    soup = BeautifulSoup(source, 'html.parser')
    models = soup.find_all('div', class_='col-lg-6 col-md-12 col-sm-12 '
                                         'col-xs-12')
    model_df = pd.DataFrame(columns=['Modell', 'Preis',
                                    'Kapazität',
                               'Spez. Kosten'])
    for model in models:
        price_id = model.find('div', class_='os_list_price1').text
        if price_id == "":
            price_id = model.find('div', class_='os_list_price2').text
            price = price_id.split('\xa0')[0]
            price = float(price.replace('.', '').replace(',', '.'))
        else:
            price = price_id.split('\xa0')[1]
            price = float(price.replace('.', '').replace(',', '.'))
        title = model.find('div', class_='os_list_title').text
        name = title.split(',')[0]
        brand = re.search(r'(Viessmann|Vaillant|Junkers|Buderus|Wolf|TWL)', name, flags=re.IGNORECASE)
        if brand is not None:
            brand = brand.group()
        else:
            brand = ''
        if equipment in ['Heizungswasserspeicher', 'Kombi-Hygienespeicher',
                         'Warmwasserspeicher']:
            match = re.search(r'\d\d\d?\d?,?\d? ?Liter', title,
                              flags=re.IGNORECASE)
        else:
            match = re.search(r'\d\d?\d?,?\d? ?kW', title, flags=re.IGNORECASE)
        if match is not None:
            capacity = match.group()
            if equipment in ['Heizungswasserspeicher',
                             'Kombi-Hygienespeicher', 'Warmwasserspeicher']:
                capacity = re.split(r'Liter', capacity, flags=re.IGNORECASE)[0]
            else:
                capacity = re.split(r'kW', capacity, flags=re.IGNORECASE)[0]
            capacity = float(capacity.replace(',', '.'))
            i_0 = price / capacity
            x = model_df[model_df["Kapazität"] == capacity]["Preis"]
            b = {'Hersteller': brand, 'Modell': name, 'Preis': price, 'Kapazität': capacity, 'Spez. Kosten': i_0}
            if x.size == 0:
                model_df = model_df.append(b, ignore_index=True)
            elif x.iloc[0] > price:
                model_df.drop(model_df.index[model_df["Kapazität"] == capacity], inplace=True)
                model_df = model_df.append(b, ignore_index=True)
        else:
            pass
    df = pd.concat([df, model_df])
    return df


base_path = os.path.dirname(os.path.abspath(__file__))
source_url = "https://www.heizungsdiscount24.de/"
equipments_list = ['Gaskessel', 'Heizungswasserspeicher',
                   'Kombi-Hygienespeicher', 'LW WP', 'SW WP',
                   'Warmwasserspeicher', 'Ölkessel']


if __name__ == "__main__":
    equipment = 'Ölkessel'
    if equipment in equipments_list:
        urls_excel = os.path.join(base_path, 'Input', equipment + " Input.xlsx")
        data = pd.read_excel(urls_excel)
        url = data['URL'].values
        df = pd.DataFrame(columns=['Hersteller', 'Modell', 'Preis', 'Kapazität',
                                   'Spez. Kosten'])
        for html in url:
            source = get_source(html)
            df = get_string(source, df, equipment)

        output_path = os.path.join(base_path, 'Output', equipment +
                                   "_output.csv")
        df.to_csv(output_path)
    else:
        warnings.warn("The input urls for equipment " + equipment + " is not "
                                                                    "found")
