"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import os
import warnings
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy.optimize import curve_fit
from sklearn.metrics import r2_score
import subprocess


equipments_list = ['Gaskessel', 'Heizungswasserspeicher',
                   'Kombi-Hygienespeicher', 'LW WP', 'SW WP',
                   'Warmwasserspeicher', 'Ölkessel', 'WW WP']
function_type_list = ['power', 'exp', 'log', 'lin', 'inverse_prop']
base_path = os.path.dirname(os.path.abspath(__file__))


def power_func(q, c0, c1, c2):
    return c0 + c1*pow(q, c2)


def lin_func(q, c0, c1):
    return c0 + q * c1


def exp_func(q, c0, c1, c2):
    return c0 + c1*np.exp(q * c2)


def log_func(q, c0, c1):
    return c0 + c1 * np.log(q)


def inverse_prop_func(q, c0, c1, c2):
    return c0 + c1 * pow(q, -1) + c2 * pow(q, -2)


def to_fit(function_type, q, i_0):
    """
    The method get the fitted result for different fit types
    :param function_type: types of fitted curves, could be chosen from
    "power", "power", "lin", "exp", "log", "inverse_prop".
    :param q: independent variable in x achse
    :param i_0: dependent data in y achse
    :return: lable for plot, which shows the readable formate of curve fit;
    fitted result for each independent variable.
    """
    fitted_data = []
    if function_type in function_type_list:
        if function_type == 'power':
            p0 = 100, 1000, -0.7
            c, cov = curve_fit(power_func, q, i_0, p0)
            c0_fit = round(c[0], 3)
            c1_fit = round(c[1], 3)
            c2_fit = round(c[2], 3)
            if c1_fit >= 0:
                plt_lab = '$y=%.3f+%.3f*x^{%.3f}$' % (c0_fit, c1_fit, c2_fit)
            elif c1_fit < 0:
                plt_lab = '$y=%.3f%.3f*x^{%.3f}$' % (c0_fit, c1_fit, c2_fit)
            for i in q:
                fitted_data.append(power_func(i, c0_fit, c1_fit, c2_fit))
        elif function_type == 'lin':
            c, cov = curve_fit(lin_func, q, i_0)
            c0_fit = round(c[0], 3)
            c1_fit = round(c[1], 3)
            if c1_fit >= 0:
                plt_lab = '$y=%.3f+%.3f*x$' % (c0_fit, c1_fit)
            elif c1_fit < 0:
                plt_lab = '$y=%.3f%.3f*x$' % (c0_fit, c1_fit)
            for i in q:
                fitted_data.append(lin_func(i, c0_fit, c1_fit))
        elif function_type == 'exp':
            p0 = 200, 1000, -0.1
            c, cov = curve_fit(exp_func, q, i_0, p0)
            c0_fit = round(c[0], 3)
            c1_fit = round(c[1], 3)
            c2_fit = round(c[2], 3)
            if c1_fit >= 0:
                plt_lab = '$y=%.3f+%.3f*e^{%.3f*x}$' % (c0_fit, c1_fit, c2_fit)
            elif c1_fit < 0:
                plt_lab = '$y=%.3f%.3f*e^{%.3f*x}$' % (c0_fit, c1_fit, c2_fit)
            for i in q:
                fitted_data.append(exp_func(i, c0_fit, c1_fit, c2_fit))
        elif function_type == 'log':
            c, cov = curve_fit(log_func, q, i_0)
            c0_fit = round(c[0], 3)
            c1_fit = round(c[1], 3)
            if c1_fit >= 0:
                plt_lab = '$y=%.3f+%.3f*log(x)$' % (c0_fit, c1_fit)
            elif c1_fit < 0:
                plt_lab = '$y=%.3f%.3f*log(x)$' % (c0_fit, c1_fit)
            for i in q:
                fitted_data.append(log_func(i, c0_fit, c1_fit))
        elif function_type == 'inverse_prop':
            c, cov = curve_fit(inverse_prop_func, q, i_0)
            c0_fit = round(c[0], 3)
            c1_fit = round(c[1], 3)
            c2_fit = round(c[2], 3)
            if c1_fit >= 0:
                plt_lab = '$y=%.3f+%.3f*x^{-1}+%.3f*x^{-2}$' % (c0_fit, c1_fit, c2_fit)
            elif c1_fit < 0:
                plt_lab = '$y=%.3f%.3f*x^{-1}+%.3f*x^{-2}$' % (c0_fit, c1_fit, c2_fit)
            if c2_fit >= 0:
                plt_lab = '$y=%.3f+%.3f*x^{-1}+%.3f*x^{-2}$' % (c0_fit, c1_fit, c2_fit)
            elif c2_fit < 0:
                plt_lab = '$y=%.3f+%.3f*x^{-1}%.3f*x^{-2}$' % (c0_fit, c1_fit, c2_fit)
            for i in q:
                fitted_data.append(inverse_prop_func(i, c0_fit, c1_fit, c2_fit))
    else:
        warnings.warn("The function " + function_type + " is not found!")

    return plt_lab, fitted_data


def saveas_emf(figure, file_name, inkscape_path="C://Program Files//Inkscape//inkscape.exe"):
    """
    The method saves figure as emf file, which calls inkscape, loads svg file
    and converts it to emf. The installation of Inkscape is necessary
    :param figure: matplotlib figure
    :param file_name: name for output emf file
    :param inkscape_path: path for installed software Inkscape
    :return:
    """

    filepath = os.path.join(base_path, 'Figures')
    svg_filepath = os.path.join(filepath, file_name+'.svg')
    emf_filepath = os.path.join(filepath, file_name+'.emf')

    figure.savefig(svg_filepath, format='svg')

    subprocess.call([inkscape_path, svg_filepath, '--export-emf', emf_filepath])
    os.remove(svg_filepath)


def to_plot(q, i_0, fit, lab, equipment, function_type):
    """
    The method plots the fitted curve.
    :param q: independent variable in x achse
    :param i_0: dependent data in y achse
    :param fit: fitted result for each independent variable.
    :param lab: lable for plot, which shows the readable formate of curve fit
    :param equipment: type of equipment for curve fitting
    :param function_type: types of fitted curves, could be chosen from
    "power", "power", "lin", "exp", "log", "inverse_prop"
    :return: plots
    """
    plt.style.use('ebc.paper')

    font_size = 10
    font_family = 'Arial'
    line_width = 1.5
    marker_size = 4
    # fig_height = 6.18
    # fig_width = 7.6
    mpl.rcParams.update({"font.size": font_size,
                         "font.family": font_family,
                         "lines.linewidth": line_width,
                         "axes.linewidth": line_width,
                         "lines.markersize": marker_size})

    plt.figure()
    plt.plot(q, fit, label=lab)
    plt.plot(q, i_0, '.', label='Data')
    plt.plot([], [], ' ', label='R² = ' + str(round(r2_score(i_0, fit), 4)))
    if equipment in ['Heizungswasserspeicher', 'Kombi-Hygienespeicher',
                     'Warmwasserspeicher']:
        plt.xlabel('Capacity [L]')
        plt.ylabel('Specific Capital Cost [€/L]')
    else:
        plt.xlabel('Capacity [kW]')
        plt.ylabel('Specific Capital Cost [€/kW]')
    plt.ylim(ymin=0, ymax=(1.1*max(i_0)))
    plt.xlim(xmin=0, xmax=(1.1*max(q)))
    plt.legend()
    plt.grid()
    plt.savefig(os.path.join(base_path, 'Figures', equipment +
                             '_fig (' + function_type + ').png'))
    filename = equipment + '_fig (' + function_type + ')'
    saveas_emf(plt.gcf(), filename)
    return plt.show()


if __name__ == "__main__":
    # Define which equipment and what function type you want to analyse
    equipment = 'SW WP'
    function_type = 'exp'

    # Data imports
    data_path = os.path.join(base_path, 'Output', equipment + '_output.csv')
    data = pd.read_csv(data_path)
    # Sort the dataframe first, otherwise it doesn't work
    result = data.sort_values('Kapazität')
    q = result['Kapazität'].values
    i_0 = result['Spez. Kosten'].values

    if equipment in equipments_list:
        lab, fit = to_fit(function_type, q, i_0)
        to_plot(q, i_0, fit, lab, equipment, function_type)
    else:
        warnings.warn("The equipment " + equipment + " is not found!")

    # err = np.sqrt(np.diag(cov))
    # print(cov)
    # print(err)
