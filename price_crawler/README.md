Website for input: https://www.heizungsdiscount24.de/

Certain brands won't be included because they dont offer "kW" in the title of the different models, which makes identifying the "Leistung" impossible.

The code always takes lowest price. If there is the same capacity in the same URL it is looking at, it will chose the cheapest one and drop the other one. This is so models with extras are left out since they would have a much higher price than the actual system itself.
