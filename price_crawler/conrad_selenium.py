"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import os
import re
import warnings
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def get_string(driver, df):
    """
    The method use browser driver to get the price for all products with
    selenium, the price data are stored into a pandas dataframe.
    :param driver: browser driver, which imitate a real browser
    :param df: dataframe for previous data, could be empty dataframe
    :return: dataframe with price information
    """
    elements = driver.find_elements_by_class_name("contentRow")
    for element in elements:
        title = element.find_element_by_class_name("product__title").text
        brand = title.split(' ')[0]

        price_id = element.find_element_by_class_name("product__oldPrice").text
        if len(price_id) != 0:
            price = price_id.split('€')[0]
            price = float(price.replace('.', '').replace(',', '.'))
        else:
            price_id = element.find_element_by_class_name("product__currentPrice").text
            price = price_id.split('€')[0]
            price = float(price.replace('.', '').replace(',', '.'))

        table_id = element.find_elements_by_class_name("attributes__element")
        for table in table_id:
            match = re.search(r'^(\d+(?:[\.\,]\d+)? (kW|W|BTU/h))', table.text, flags=re.IGNORECASE)
            if match is not None:
                capacity_id = match.group()

        capacity_num = capacity_id.split(' ')[0]
        capacity_btu = capacity_num.replace('.', '')
        capacity_kw = capacity_num.replace(',', '.')
        if re.search(r'BTU/h', capacity_id, flags=re.IGNORECASE) is not None:
            capacity = round(float(capacity_btu) * 0.293071, 2)
        elif re.search(r'kW', capacity_id, flags=re.IGNORECASE) is not None:
            capacity = round(float(capacity_kw) * 1000, 2)
        elif re.search(r' W', capacity_id, flags=re.IGNORECASE) is not None:
            capacity = float(capacity_num)
        else:
            capacity = 0
            warnings.warn("Capacity could not be identified")
        if capacity > 0:
            i_0 = round(price / capacity, 2)
            df = df.append({'Hersteller': brand, 'Preis': price, 'Modell': title,
                            'Kapazität [W]': capacity, 'Spez. Kosten [€/W]': i_0}, ignore_index=True)
        else:
            pass

    return df


base_path = os.path.dirname(os.path.abspath(__file__))
output_path = os.path.join(base_path, 'Output', "Conrad_output.csv")
chrome_path = "C:\Program Files (x86)\chromedriver.exe"

if __name__ == "__main__":
    df = pd.DataFrame(columns=['Hersteller', 'Modell', 'Preis', 'Kapazität [W]',
                               'Spez. Kosten [€/W]'])
    pages = 2
    for page_no in range(1, pages+1):
        url = "https://www.conrad.de/de/o/klima-splitgeraet-0812051.html?page=" + str(page_no) + \
                                "&tfo_productType=Split-Klimagerät"
        driver = webdriver.Chrome(chrome_path)
        driver.get(url)

        link = WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.CLASS_NAME, "cmsCookieNotification__button__label")))
        link.click()
        df = get_string(driver, df)

    df.to_csv(output_path)
    driver.quit()
