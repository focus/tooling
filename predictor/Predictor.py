import numpy as np
import pandas as pd
import math
from Tooling.dynamics.Dynamic import Dynamic

# ----------------------------------------------------------------------------------------------------------------------
# Functions which can be used as predictors for rolling horizon steps in timeseries
# ----------------------------------------------------------------------------------------------------------------------

class Predictor:
    """
    Can be used to predict rolling horizon time steps. It holds the original time series, as well as a name and the
    prediction method that will be used for the time step prediction.

    Parameters
    ----------
    profile:
        The original profile that should be used for predictions.
    name: str
        Does not have to be unique, should be used so that the nature of the time series can be identified.
    method: str
        Name of the prediction method to be used. The default method is "same_as_last_day".
    """

    def __init__(self, profile, type: str, method: str, dynamic: Dynamic):
        self.profile = profile
        self.type = type
        self.method = method
        self.dynamic = dynamic
    
    def predict(self, time_steps):
        if self.method == "perfect_foresight":
            return self.profile[time_steps]
        elif self.method == "time_forward":
            if 0 in time_steps:
                print('Requested time forward prediction for time steps that include the first time step! Using original data for the first time step.')
                return pd.Series(self.profile[time_steps[0]], index = time_steps)
            else:
                raise("Requested a prediction, this feature is currently not supported, so use original data!")
                return pd.Series(self.profile[time_steps[0]] - 1, index = time_steps)
        elif self.method == "same_as_last_day":
            time_steps_per_day = int(24 / self.t_step)
            if time_steps[0] - time_steps_per_day < 0:
                print('Requested same as last day prediction for time steps that include the first day. Using original data for the first day.')
                previous_day_data = np.zeros(time_steps_per_day)
                previous_day_data[:time_steps_per_day - time_steps[0]] = self.profile[time_steps[0]:time_steps_per_day]
                previous_day_data[time_steps_per_day - time_steps[0]:] = self.profile[:time_steps[0]]
            else:
                raise("Requested a prediction, this feature is currently not supported, so use original data!")
                previous_day_data = np.array(self.profile[time_steps[0] - len(time_steps_per_day):time_steps[0]])
            days_in_prediction = [(t * time_steps_per_day, (t + 1) * time_steps_per_day) for t in range(math.ceil(len(time_steps) / time_steps_per_day))]
            days_in_prediction[-1] = (days_in_prediction[-1][0], time_steps[-1] - time_steps[0] + 1)
            
            prediction = pd.Series(0.0, index = time_steps)
            for start, end in days_in_prediction:
                prediction[start:end] = previous_day_data[0:end - start]
            return prediction
        elif self.method == "same_as_last_week":
            time_steps_per_week = int(7 * 24 / self.t_step)
            if time_steps[0] - time_steps_per_week < 0:
                print('Requested same as last week prediction for time steps that include the first week. Using original data for the first week.')
                previous_week_data = np.zeros(time_steps_per_week)
                previous_week_data[:time_steps_per_week - time_steps[0]] = self.profile[time_steps[0]:time_steps_per_week]
                previous_week_data[time_steps_per_week - time_steps[0]:] = self.profile[:time_steps[0]]
            else:
                raise("Requested a prediction, this feature is currently not supported, so use original data!")
                previous_week_data = np.array(self.profile[time_steps[0] - len(time_steps_per_week):time_steps[0]])
            weeks_in_prediction = [(t * time_steps_per_week, (t + 1) * time_steps_per_week) for t in range(math.ceil(len(time_steps) / time_steps_per_week))]
            weeks_in_prediction[-1] = (weeks_in_prediction[-1][0], time_steps[-1] - time_steps[0] + 1)
            
            prediction = pd.Series(0.0, index = time_steps)
            for start, end in weeks_in_prediction:
                prediction[start:end] = previous_week_data[0:end - start]
            return prediction
        else:
            return self.profile[time_steps]
