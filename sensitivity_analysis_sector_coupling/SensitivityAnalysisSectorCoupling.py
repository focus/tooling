"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import pandas as pd
import math
# from prosumer_library.model.BaseProsumer import BaseProsumer          # import BaseProsumer class


class SensitivityAnalysisSectorCoupling:          # sensitivity analysis class
    def __init__(self, results, scenario_number, max_number, reference_scenario, compare_eva_area, co2_power, co2_gas, i, t_ref,
                 __component_properties, __regulations):
        self.results = results
        self.scenario_number = scenario_number
        self.max_number = max_number
        self.reference_scenario = reference_scenario
        self.compare_eva_area = compare_eva_area
        self.co2_power = co2_power  # in kgCO2/ kWh
        self.co2_gas = co2_gas  # in kgCO2/ kWh
        self.i = i                 # interest rate (for annuity calculation)
        self.t_ref = t_ref         # runtime of the investment (for annuity calculation)
        self.__component_properties = __component_properties
        self.__regulations = __regulations

    def _evaluate_sector_coupling(self, evaluate):
        results = self.results[0]      # get value from dictionary, dataframe was nested in a dictionary
        print(self.__component_properties)
        if 'co2' in evaluate:
            self._evaluate_co2(results)
        if 'autarky' in evaluate:
            self._evaluate_autarky(results)
        if 'annuity' in evaluate:
            self._evaluate_annuity(results)
        if 'co2' in self.compare_eva_area:
            self._compare_co2()
        if 'autarky' in self.compare_eva_area:
            self._compare_autarky()
        if 'co2_cost' in self.compare_eva_area:
            self._compare_co2_cost()

    def _evaluate_co2(self, results):
        # values
        # grid and gas grid to ...
        if ('gas_grd', 'gas_boi') in results:
            gas_grd = results[('gas_grd', 'gas_boi')].sum()
        else:
            gas_grd = 0
        if ('grd', 'elec_cns') in results:
            grd_elec_cns = results[('fc', 'water_tes')].sum()
        else:
            grd_elec_cns = 0
        # inv to ...
        if ('inv', 'elec_cns') in results:
            inv_elec_cns = results[('inv', 'elec_cns')].sum()
        else:
            inv_elec_cns = 0
        # elec_cns to ...
        if ('elec_cns', 'elec_dmd') in results:
            elec_cns_elec_dmd = results[('elec_cns', 'elec_dmd')].sum()
        else:
            elec_cns_elec_dmd = 0
        if ('elec_cns', 'bat') in results:
            elec_cns_bat = results[('elec_cns', 'bat')].sum()
        else:
            elec_cns_bat = 0
        if ('elec_cns', 'electroly') in results:
            elec_cns_electroly = results[('elec_cns', 'electroly')].sum()
        else:
            elec_cns_electroly = 0
        if ('elec_cns', 'elec_boi') in results:
            elec_cns_elec_boi = results[('elec_cns', 'elec_boi')].sum()
        else:
            elec_cns_elec_boi = 0
        if ('elec_cns', 'elec_rad') in results:
            elec_cns_elec_rad = results[('elec_cns', 'elec_rad')].sum()
        else:
            elec_cns_elec_rad = 0
        if ('elec_cns', 'ehp') in results:
            elec_cns_elec_rad = results[('elec_cns', 'ehp')].sum()
        else:
            elec_cns_elec_rad = 0
        # emissions electricity
        grd_supply = results[('grd', 'elec_cns')].sum()
        total_co2_elec = results[('grd', 'elec_cns')].sum()*self.co2_power
        # average_co2_per_elec = (results['elec_cns', 'elec_dmd'])/(results[('grd', 'elec_cns')].sum()*self.co2_power)
        # emissions gas
        total_co2_gas = results[('gas_grd', 'gas_boi')].sum() * self.co2_gas
        # emissions heat
        total_co2_heat = gas_grd * self.co2_gas + (inv_elec_cns + grd_elec_cns - elec_cns_elec_dmd) * self.co2_power
        #print(inv_elec_cns*self.co2_power)
        #print(grd_elec_cns*self.co2_power)
        #print(elec_cns_elec_dmd*self.co2_power)

        #average_co2_per_heat =(results[('water_tes','water_cns')]+results[('water_tes','therm_cns')])/\
        #                      (results[('gas_grd', 'gas_boi')].sum() * self.co2_gas)

        data = {'indicator': ['grd_supply', 'total_co2_elec', 'total_co2_gas', 'total_co2_heat'],
                'value': [grd_supply, total_co2_elec, total_co2_gas, total_co2_heat],
                'unit': ['kWh', 'kgCO2', 'kgCO2', 'kgCO2']}
        # create DataFrame
        df = pd.DataFrame(data)
        # write DataFrame to excel sheet
        df.to_excel('output_files/'+str(self.scenario_number)+'_SensAnalySC_co2.xlsx')

    def _evaluate_autarky(self, results):
        # values from results
        # grid and gas grid to ...
        if ('gas_grd', 'gas_boi') in results:
            gas_grd = results[('gas_grd', 'gas_boi')].sum()
        else:
            gas_grd = 0
        if ('grd', 'elec_cns') in results:
            grd_elec_cns = results[('fc', 'water_tes')].sum()
        else:
            grd_elec_cns = 0
        # inv to ...
        if ('inv', 'elec_cns') in results:
            inv_elec_cns = results[('inv', 'elec_cns')].sum()
        else:
            inv_elec_cns = 0
        # elec_cns to ...
        if ('elec_cns', 'elec_dmd') in results:
            elec_cns_elec_dmd = results[('elec_cns', 'elec_dmd')].sum()
        else:
            elec_cns_elec_dmd = 0
        if ('elec_cns', 'bat') in results:
            elec_cns_bat = results[('elec_cns', 'bat')].sum()
        else:
            elec_cns_bat = 0
        if ('elec_cns', 'electroly') in results:
            elec_cns_electroly = results[('elec_cns', 'electroly')].sum()
        else:
            elec_cns_electroly = 0
        if ('elec_cns', 'elec_boi') in results:
            elec_cns_elec_boi = results[('elec_cns', 'elec_boi')].sum()
        else:
            elec_cns_elec_boi = 0
        if ('elec_cns', 'elec_rad') in results:
            elec_cns_elec_rad = results[('elec_cns', 'elec_rad')].sum()
        else:
            elec_cns_elec_rad = 0
        if ('elec_cns', 'ehp') in results:
            elec_cns_ehp = results[('elec_cns', 'ehp')].sum()
        else:
            elec_cns_ehp = 0
        # heat
        if ('gas_boi', 'water_tes') in results:
            gas_boi_water_tes = results[('gas_boi', 'water_tes')].sum()
        else:
            gas_boi_water_tes = 0
        # autarky for 1 year
        # energy-based autarky
        ea_power = (1 - (grd_elec_cns / (elec_cns_elec_dmd + elec_cns_bat + elec_cns_electroly + elec_cns_elec_boi
                                         + elec_cns_elec_rad + elec_cns_ehp))) * 100
        ea_gas = (1 - gas_boi_water_tes /
                  (results[('water_tes', 'water_cns')].sum() + results[('water_tes', 'therm_cns')].sum())) * 100
        # self consumption rate (pv power)
        scr = (results[('inv', 'elec_cns')].sum() / (
                    results[('inv', 'elec_cns')].sum() + results[('inv', 'grd')].sum())) * 100
        # self sufficiency rate (pv power)
        ssr = (results[('inv', 'elec_cns')].sum() /
               (results[('inv', 'elec_cns')].sum() + results[('grd', 'elec_cns')].sum())) * 100
        data = {'indicator': ['energy-based autarky power', 'energy-based autarky natural gas', 'self consumption rate',
                              'self sufficiency rate'],
                'value': [ea_power, ea_gas, scr, ssr],
                'unit': ['%', '%', '%', '%']}
        # create DataFrame
        df = pd.DataFrame(data)
        # write DataFrame to excel sheet
        df.to_excel('output_files/'+str(self.scenario_number)+'_SensAnalySC_autarky.xlsx')

    def _evaluate_annuity(self, results):
        elec = pd.read_csv('input_files/elec_matrix.csv')
        gas = pd.read_csv('input_files/gas_matrix.csv')
        therm = pd.read_csv('input_files/therm_matrix.csv')
        hydrog = pd.read_csv('input_files/hydrog_matrix.csv')
        elec_comp = elec.loc[:, 'component']
        gas_comp = gas.loc[:, 'component']
        therm_comp = therm.loc[:, 'component']
        hydrog_comp = hydrog.loc[:, 'component']
        comp = pd.concat([elec_comp, gas_comp, therm_comp, hydrog_comp], axis=0)    # all components in one dataframe
        comp = comp[~comp.index.duplicated()]                                       # remove component duplicates
        comp = pd.Series.tolist(comp)
        #print(comp)
        all_comp = pd.concat([elec, gas, therm, hydrog], axis=0)
        all_comp = all_comp[~all_comp.index.duplicated()]
        all_comp = all_comp.set_index(['component'])
        # prepare total cost dataframe
        all_cost = pd.DataFrame(columns=['cost'], index=comp)
        # check which components are used, read the values and calculate total investment costs of each component
        for n in comp:
            type = all_comp['type'][str(n)]
            model = all_comp['model'][str(n)]
            data = pd.read_csv('Model_library/Component/data/' + type + '/' + model + '.csv')
            inv_cost = self.get_inv_cost(data)
            fix_cost_factor = self.get_fix_cost_factor(data)
            size = self.get_size(results, n)
            cost = inv_cost + fix_cost_factor * size
            all_cost.loc[str(n), 'cost'] = cost

        total_cost = all_cost['cost'].sum()
        f_an = self.annuity_factor()  # get annuity factor
        an = total_cost * f_an      # calculate total annuity
        # ToDo: calculate annuity of the individual components
        # running costs
        power_grid_costs = results[('ElectricityPrice')]*results[('grd', 'elec_cns')]         # power from the grid
        power_grid_costs = power_grid_costs.sum()
        # natural gas from the grid
        gas_price = self.__regulations.get('gas_price')
        gas = results[('gas_grd', 'gas_boi')].sum()
        gas_costs = gas * gas_price
        # fed-in power
        injection_price = self.__regulations.get('injection_price')
        injection_power = results[('inv', 'grd')].sum()
        injection_return = injection_price * injection_power
        # yearly cost
        yearly_cost = an + power_grid_costs + gas_costs + injection_return
        # prepare annuity dataframe
        annuity = pd.DataFrame(columns=['annuity'], index=[self.scenario_number])       # prepare annuity dataframe
        annuity.loc[self.scenario_number, 'annuity'] = an       # write annuity into dataframe
        annuity.loc[self.scenario_number, 'power_grid_costs in €'] = power_grid_costs  # write power_grid_costs into dataframe
        annuity.loc[self.scenario_number, 'gas_costs in €'] = gas_costs     # write gas_costs into dataframe
        annuity.loc[self.scenario_number, 'injection return in €'] = injection_return
        annuity.loc[self.scenario_number, 'yearly_cost in €'] = yearly_cost

        # write annuity to excel sheet
        annuity.to_excel('output_files/'+str(self.scenario_number)+'_SensAnalySC_annuity.xlsx', engine='openpyxl')

    def _compare_co2(self):
        # create dataframe for comparison
        data = {'indicator': ['grd_supply', 'total_co2_elec', 'total_co2_gas', 'total_co2_heat'],
                'unit': ['kWh', 'kgCO2', 'kgCO2', 'kgCO2']}
        number = 1
        co2_df = pd.DataFrame(data)
        #print(co2_df)
        # read results from excel and write results into dataframe
        while number <= self.max_number:

            co2_data = pd.read_excel('output_files/'+str(number)+'_SensAnalySC_co2.xlsx', engine='openpyxl')
            data = co2_data.loc[:, 'value']
            co2_df[number] = data

            number = number + 1

        #print(co2_df)
        # write DataFrame to excel sheet
        co2_df.to_excel('output_files/Compare_SensAnalySC_co2.xlsx', engine='openpyxl')

    def _compare_autarky(self):
        # create dataframe for comparison
        data = {'indicator': ['energy-based autarky power', 'energy-based autarky natural gas', 'self consumption rate',
                              'self sufficiency rate'],
                'unit': ['%', '%', '%', '%']}
        number = 1
        autarky_df = pd.DataFrame(data)
        # read results from excel and write results into dataframe
        while number <= self.max_number:
            autarky_data = pd.read_excel('output_files/' + str(number) + '_SensAnalySC_autarky.xlsx', engine='openpyxl')
            data = autarky_data.loc[:, 'value']
            autarky_df[number] = data
            number = number + 1
        # write DataFrame to excel sheet
        autarky_df.to_excel('output_files/Compare_SensAnalySC_autarky.xlsx', engine='openpyxl')

    def _compare_co2_cost(self):
        # prepare co2_cost dataframe
        co2_cost = pd.DataFrame(columns=['cost in €', 'co2 in kg', '€/kgco2'])
        number = 1
        while number <= self.max_number:
            annuity_data = pd.read_excel('output_files/' + str(number) + '_SensAnalySC_annuity.xlsx', engine='openpyxl')
            yearly_cost = annuity_data.loc[0]['yearly_cost in €']
            co2_cost.loc[str(number), 'cost in €'] = yearly_cost        # write yearly_cost into dataframe
            co2_data = pd.read_excel('output_files/' + str(number) + '_SensAnalySC_co2.xlsx', engine='openpyxl')
            co2_data = co2_data.set_index(['indicator'])
            total_co2_elec = co2_data.loc['total_co2_elec']['value']
            total_co2_heat = co2_data.loc['total_co2_heat']['value']
            total_co2 = total_co2_elec + total_co2_heat            # sum up total_co2_elec and total_co2_heat
            co2_cost.loc[str(number), 'co2 in kg'] = total_co2       # write co2 into co2_cost dataframe
            euro_co2 = yearly_cost/total_co2
            co2_cost.loc[str(number), '€/kgco2'] = euro_co2
            # co2 mitigation costs
            if self.reference_scenario not in list(range(self.max_number + 1)):
                self.reference_scenario = 1
            co2_mitigation = co2_cost.loc[str(number), '€/kgco2'] - co2_cost.loc[str(self.reference_scenario), '€/kgco2']
            co2_cost.loc[str(number), 'co2_mitigation costs in €/kWh'] = co2_mitigation  # write co2_mitigation into dataframe
            number = number + 1

        # write co2_cost dataframe to excel
        co2_cost.to_excel('output_files/Compare_SensAnalySC_co2_cost.xlsx', engine='openpyxl')
            # if self.reference_scenario==[]:
            # read from excel annuity und co2.emissions (from all models)
            # annuity data aller szenarien in dataframe (1. Spalte)
            # co2 data aller szenarien in dataframe (2. Spalte)
            # annuity/co2 in dataframe (3.Spalte)

    """# prepare annuity dataframe
        annuity = pd.DataFrame(columns=['annuity'], index=[self.scenario_number])
        # write annuity into dataframe
        annuity.loc[self.scenario_number, 'annuity'] = an
        # write annuity to excel sheet
        annuity.to_excel('output_files/'+str(self.scenario_number)+'_SensAnalySC_annuity.xlsx', engine='openpyxl')"""


    def get_inv_cost(self, data):
        if 'inv_cost' in data:
            inv_cost = data.loc[:, 'inv_cost']
            inv_cost = inv_cost[0]
        else:
            inv_cost = 0
        return inv_cost

    def get_fix_cost_factor(self, data):
        if 'fix_cost_factor' in data:
            fix_cost_factor = data.loc[:, 'fix_cost_factor']
            fix_cost_factor = fix_cost_factor[0]
        else:
            fix_cost_factor = 0
        return fix_cost_factor

    def get_size(self, results, n):
        if ('size', str(n)) in results.keys():
            size = results[('size', str(n))][1]
        else:
            size = 0
        return size

    def annuity_factor(self):
        f_an = (math.pow((1+self.i), self.t_ref) * self.i)/(math.pow((1+self.i), self.t_ref)-1)
        return f_an


