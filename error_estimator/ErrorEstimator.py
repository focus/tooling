"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import numpy as np
import pandas as pd
import os
import openpyxl

class ErrorEstimator:
    """
    Collection of methods to estimate errors caused by temporal aggregation.

    The main functionalities are as follows:
    1.  Calculating errors and error_flows for the scenario given.
    2.  Writing the calculated values to excel-files.

    Attributes:
        __keys_dict: Holds the lists with the sought optimization parameters (keys from the result-dict) for each prosumer.
        __errors: Nested dictionary to hold the calculated error values for the optimized.
                parameters as follows __errors[agg_day][prosumer][rsl][error_type][component].
        __errorsflow: Nested dictionary to hold the calculated error values of the flow variables over the time steps.
                For this, all the results over time for one variable/component in the scenario is interpreted as a vector such that
                the calculated error value is the error between two vectors. Nesting scheme same as for __errors.
    """

    def __init__(self, key_dict):
        """
        Initializes the class with list of sought optimization parameters.

        Arguments:
            key_dict: This dictionary contains a list of the sought optimization parameters for each prosumer.
        """
        self.__keys_dict = key_dict   # TODO: when the use of rsl is clear, it may be necessary to have different opt parameters for each rsl (not only prosumer)
        self.__errors = {}
        self.__errorsflow = {}
        self.__extrapolated_agg_sol = {}

    ############################################
    # public:

    def error(self, reference, aggregated, prosumer_name_list, agg_days):
        """
        This method calculates the different error values (rmse, mae, relative error)
        between the reference and the aggregated scenario
        for the keys given in self.__keys_dict for each prosumer in the prosumer_name_list.
        The values are saved internally in __errors[agg_days].

        Arguments:
            reference: Instance of the Main class as reference scenario.
            aggregated: Instance of the Main class of the same scenario as reference but with aggregated days.
            prosumer_name_list: List of the prosumer in the scenario.
            agg_days: Number of days to which the aggregated scenario was aggregated to.

        Returns:
            __errors[agg_days]: A nested dictionary conatining the calculated error values for each prosumer.

            - None: If the prosumer list provided does not match the scenario or if all keys in self.__keys_dict for all prosumers are invalid.
            - Nesting of returned dict: dict[prosumer][rsl][error_type][component]
                - dict[prosumer] may be empty if the keys provided in __keys_dict[prosumer] do not match the prosumer from the scenario.

            Note: The relative error is only defined for scalar values with reference value unequal to zero.
        """
        # Verify that the prosumer list provided matches the reference; if not, delete the invalid prosumers from the list.
        invalid_prosumer = self.__find_invalid_prosumer(prosumer_name_list, reference)
        for elem in invalid_prosumer:
            print("\033[0;33mWarning:\033[0;0m The prosumer \"{}\" given in the prosumer_name_list does not matche the given scenario.".format(elem))
            print("Deleting prosumer \"{}\" from the prosumer_name_list.".format(elem))
            prosumer_name_list.remove(elem)
            print("Continuing without prosumer \"{}\".".format(elem))

        # Initialize empty dictionary for the given aggregation days in __errors.
        self.__errors[agg_days] = {}

        # Calculate the error types (rmse, mae, relative error) for each component in each rsl for each prosumer.
        for eachprosumer in prosumer_name_list:
            result_ref = reference.prosumer[eachprosumer].get_results()         # Solution without aggregation.
            result_agg = aggregated.prosumer[eachprosumer].get_results()        # Solution with aggregation.
            self.__errors[agg_days][eachprosumer] = {}                          # Initialize empty dict for the prosumer.

            # Calculate the error types for each component for each rsl of the current prosumer.
            for rsl in result_ref:

                # Verify that the given key_list of optimized parameters for the prosumer matches the reference; if not skip this rsl.
                invalid_keys = self.__find_invalid_keys(result_ref[rsl], eachprosumer)
                for elem in invalid_keys:
                    print("\033[0;33mWarning:\033[0;0m The key \"{}\" given for prosumer \"{}\" and rsl \"{}\" does not match the given scenario.".format(elem,eachprosumer,rsl))
                    print("Deleting key \"{}\" from __keys_dict[{}]...".format(elem, eachprosumer))
                    self.__keys_dict[eachprosumer].remove(elem)
                    print("Continuing without key \"{}\".".format(elem))
                # After deleting invalid keys, the key_list for the given prosumer could be empty
                if not self.__keys_dict[eachprosumer]:
                    continue

                # Calculate the rmse, mae and rel for each component between the reference and the aggregated scenario.
                rmse= {}
                mae = {}
                rel = {}
                for key in self.__keys_dict[eachprosumer]:
                    error = abs(result_ref[rsl][key][0] - result_agg[rsl][key][0]) #RMSE of a single value is same as MAE
                    if result_ref[rsl][key][0]==0:
                        rel[key] = np.NaN
                    else:
                        rel_error = (result_agg[rsl][key][0] - result_ref[rsl][key][0])/result_ref[rsl][key][0]
                        rel[key] = rel_error
                    rmse[key] = error
                    mae[key] = error

                # Calculate the total rmse and mae if the optimized values are interpreted as vectors
                rmse['total'] = ((((result_ref[rsl][self.__keys_dict[eachprosumer]].iloc[0,:] - result_agg[rsl][self.__keys_dict[eachprosumer]].iloc[0,:])) ** 2).mean()) ** .5
                mae['total'] = (result_ref[rsl][self.__keys_dict[eachprosumer]].iloc[0,:] - result_agg[rsl][self.__keys_dict[eachprosumer]].iloc[0,:]).abs().mean()
                rel['total'] = np.NaN

                # Save the calculated values in the internal dictionary of the class
                self.__errors[agg_days][eachprosumer][rsl] = {'rmse': rmse, 'mae': mae, 'rel': rel}

            #if no valid keys were provided for the prosumer, no results were calculated and the prosumer_key can be deleted
            if not self.__errors[agg_days][eachprosumer]:
                print("\033[0;33mWarning:\033[0;0m No valid keys were provided for the prosumer \"{}\".".format(eachprosumer))
                print("Deleting the prosumer \"{}\" from the error dictionary self.__errors[{}] since no errors could be calculated...".format(eachprosumer, agg_days))
                self.__errors[agg_days].pop(eachprosumer)
                print("Continuing.")

        #if all keys for all prosumers were invalid and/or invalid prosumer were provided, no calculations were done for the aggregation day and the agg_day_key can be deleted
        if not self.__errors[agg_days]:
            print("\033[0;33mWarning:\033[0;0m No errors could be calculated for the number of aggregation days \"{}\".".format(agg_days))
            print("Deleting the aggregation day \"{}\" from the error dictionary self.__errors since no errors could be calculated...".format(agg_days))
            self.__errors.pop(agg_days)
            print("Continuing.")
            return None

        return self.__errors[agg_days]


    def error_flow(self, reference, aggregated, prosumer_name_list, agg_days):
        """
        This method calculates the different error values (rmse, mae)
        between the reference and the aggregated scenario
        for all flow variables over the time steps in the scenario.
        For one flow variable/component the different results over time are interpreted as a vector
        such that the calculated error for this component is the error between two vectors.
        The values are saved internally in __errorsflow[agg_days].

        Args:
            reference: Instance of the Main class as reference scenario.
            aggregated: Instance of the Main class of the same scenario as reference but with aggregated days.
            prosumer_name_list: List of the prosumer in the scenario.
            agg_days: Number of days to which the aggregated scenario was aggregated to.

        Returns:
            __errorsflow[agg_days]: A nested dictionary conatining the calculated error values for each prosumer.

            - None: If the prosumer list provided does not match the scenario.
            - Nesting of returned dict: dict[prosumer][rsl][error_type][component]
        """
        from numbers import Number

        # Verify that the prosumer list provided matches the reference; if not, delete the invalid prosumers from the list.
        invalid_prosumer = self.__find_invalid_prosumer(prosumer_name_list, reference)
        for elem in invalid_prosumer:
            print("\033[0;33mWarning:\033[0;0m The prosumer \"{}\" given in the prosumer_name_list does not matche the given scenario.".format(elem))
            print("Deleting prosumer \"{}\" from the prosumer_name_list.".format(elem))
            prosumer_name_list.remove(elem)
            print("Continuing without prosumer \"{}\"".format(elem))

        # Initialize empty dictionary for the given aggregation days in __errorsflow.
        self.__errorsflow[agg_days] = {}

        # Calculate the error types (rmse, mae, relative error) for each component in each rsl for each prosumer.
        for eachprosumer in prosumer_name_list:
            result_ref = reference.prosumer[eachprosumer].get_results()         # Solution without aggregation.
            result_agg = aggregated.prosumer[eachprosumer].get_results()        # Solution with aggregation.
            self.__errorsflow[agg_days][eachprosumer] = {}                      # Inititalize empty dict for the prosumer.

            # Do indexmatching of the results to subsequently extend the aggregated scenario to the length of the reference.
            # The periodOrder tells which aggregated day represents which days in the reference (several).
            # A list with indexes of the order of the typical periods in hours is created.
            # Note: This implementation does not take Segmentation into account, the resolution in a day would have to be taken into account
            index_list = []
            step = aggregated.hoursPerPeriod
            for i in aggregated.periodOrder:
                index_list.extend([x for x in range(0+i*step,step+i*step)])
                #index_list.extend([x for x in (range(step)+i*step)])


            # Calculate the error types for each component for each rsl of the current prosumer
            for rsl in result_ref:

                # Adapt the aggregated solution to have the same length as the reference via the index_list
                result_agg_complete = result_agg[rsl].iloc[index_list].reset_index(drop=True)

                # Calculate the error values for all the components in rsl
                rmse = {}
                mae = {}
                for key in result_ref[rsl].iloc[:,1:].columns:
                    diff=pd.concat([result_agg_complete[key],result_ref[rsl][key]], axis=1)
                    # Check if the first value of the column is a Number
                    if not isinstance(diff.iloc[:,0][0], Number):
                        rmse[key] = np.NaN
                        mae[key] = np.NaN
                        continue
                    rmse[key]=(((diff.iloc[:,0] - diff.iloc[:,1]) ** 2).mean()) ** .5
                    mae[key] = (diff.iloc[:,0] - diff.iloc[:,1]).abs().mean()

                # Save the calculated values in the internal dictionary of the class
                self.__errorsflow[agg_days][eachprosumer][rsl] = {'rmse': rmse, 'mae': mae}

        #if only invalid prosumers were provided, no calculations were done for the aggregation day and the agg_day_key can be deleted
        if not self.__errorsflow[agg_days]:
            print("\033[0;33mWarning:\033[0;0m No errors could be calculated for the number of aggregation days \"{}\".".format(agg_days))
            print("Deleting the aggregation day \"{}\" from the error dictionary self.__errorsflow since no errors could be calculated...".format(agg_days))
            self.__errorsflow.pop(agg_days)
            print("Continuing.")
            return None

        return self.__errorsflow[agg_days]


    def extrapolate_agg_sol(self, reference, aggregated, prosumer_name_list, agg_days):
        """
        This method extrapolates the results from the aggregated scenario to the length of the reference scenario
        for each prosumer provided in the prosumer list and each rsl for the specific prosumer.
        Knowledge about the order (i.e. which aggregation day represents which days in the reference scenario)
        is provided by the Main class and used here.

        Args:
            reference: Instance of the Main class as reference scenario.
            aggregated: Instance of the Main class of the same scenario as reference but with aggregated days.
            prosumer_name_list: List of the prosumer in the scenario.
            agg_days: Number of days to which the aggregated scenario was aggregated to.

        Returns:
            __extrapolated_agg_sol[agg_days]: A nested dictionary conatining the extrapolated results for the aggregation day provided.
                - None: If the prosumer list provided does not match the scenario.
                - Nesting of returned dict: dict[prosumer][rsl]
        """

        invalid_prosumer = self.__find_invalid_prosumer(prosumer_name_list, reference)
        for elem in invalid_prosumer:
            print("\033[0;33mWarning:\033[0;0m The prosumer \"{}\" given in the prosumer_name_list does not matche the given scenario.".format(elem))
            print("Deleting prosumer \"{}\" from the prosumer_name_list.".format(elem))
            prosumer_name_list.remove(elem)
            print("Continuing without prosumer \"{}\"".format(elem))
        if not prosumer_name_list:
            print("\033[0;31mError:\033[0;0m The list of prosumers is empty.")
            return None

        # Initialize empty dictionary for the given aggregation days in __agg_extrapolated.
        self.__extrapolated_agg_sol[agg_days] = {}

        # Calculate the error types (rmse, mae, relative error) for each component in each rsl for each prosumer.
        for eachprosumer in prosumer_name_list:
            result_ref = reference.prosumer[eachprosumer].get_results()         # Solution without aggregation.
            result_agg = aggregated.prosumer[eachprosumer].get_results()        # Solution with aggregation.
            self.__extrapolated_agg_sol[agg_days][eachprosumer] = {}            # Inititalize empty dict for the prosumer.

            # Do indexmatching of the results to subsequently extend the aggregated scenario to the length of the reference.
            # The periodOrder tells which aggregated day represents which days in the reference (several).
            # A list with indexes of the order of the typical periods in hours is created.
            # Note: This implementation does not take Segmentation into account, the resolution in a day would have to be taken into account
            # Note: A periodOrder for each prosumer (and or rsl) may be necessary (provided by Main class)
            index_list = []
            step = aggregated.hoursPerPeriod
            for i in aggregated.periodOrder:
                index_list.extend([x for x in range(0+i*step,step+i*step)])

            # Extrapolate the aggregated results for each rsl of the current prosumer
            for rsl in result_ref:

                # Adapt the aggregated solution to have the same length as the reference via the index_list and save it in __agg_extrapolated.
                self.__extrapolated_agg_sol[agg_days][eachprosumer][rsl] = result_agg[rsl].iloc[index_list].reset_index(drop=True)

        return self.__extrapolated_agg_sol[agg_days]


    def write_err2excel(self, outputfile, option = "error"):
        """
        This method writes the complete error dictionary to the excel-file given.

        Args:
            outputfile: Path to the excel-file to write the errors to (.xlsx or .xlsm).
            option: Decides which error dictionary shall be written to excel.
            - possible values: "error" (__errors is written), "errorflow" (__errorsflow is written), "all" (both are written)
            - default: "error"
        """
        if option == "error":
            for agg_day in sorted(self.__errors.keys()):
                self.write_aggday_err2excel(agg_day, outputfile)
        elif option == "errorflow":
            for agg_day in sorted(self.__errorsflow.keys()):
                self.write_aggday_err2excel(agg_day, outputfile, "errorflow")
        elif option == "all":
            keys = self.__get_agg_day_options()
            for agg_day in sorted(keys.keys()):
                self.write_aggday_err2excel(agg_day, outputfile, keys[agg_day])
        else:
            print("\033[0;31mError:\033[0;0m Unvalid option \"{}\". Valid options are \"error\", \"errorflow\" and \"all\".".format(option))


    def write_aggday_err2excel(self, agg_day, outputfile, option = "error"):
        """
        This method writes the error dictionary for a specific number of aggregation days to the excel-file given.

        Args:
            agg_day: Number of days used for the aggregation.
            outputfile: Path to the excel-file to write the errors to (.xlsx or .xlsm).
            option: Decides which error dictionary shall be written to excel.
            - possible values: "error" (__errors is written), "errorflow" (__errorsflow is written), "all" (both are written)
            - default: "error"
        """
        if option == "error":
            if agg_day in self.__errors.keys():
                for eachprosumer in self.__errors[agg_day].keys():
                    self.write_prosumer_err2excel(agg_day, eachprosumer, outputfile)
            else:
                print("\033[0;31mError:\033[0;0m Unvalid option. Error is not calculated for the aggregation day \"{}\" given.".format(agg_day))
                return
        elif option == "errorflow":
            if agg_day in self.__errorsflow.keys():
                for eachprosumer in self.__errorsflow[agg_day].keys():
                    self.write_prosumer_err2excel(agg_day, eachprosumer, outputfile, "errorflow")
            else:
                print("\033[0;31mError:\033[0;0m Unvalid option. Errorflow is not calculated for the aggregation day \"{}\" given.".format(agg_day))
        elif option == "all":
            option_valid = self.__verify_option_all(agg_day)
            if not option_valid:
                print("\033[0;31mError:\033[0;0m Unvalid option. Not both errors, error and errorflow, were calculated for the aggregation day \"{}\" given.".format(agg_day))
                return

            keys = self.__get_prosumer_options(agg_day)
            for prosumer in keys.keys():
                self.write_prosumer_err2excel(agg_day, prosumer, outputfile, keys[prosumer])
        else:
            print("\033[0;31mError:\033[0;0m Unvalid option \"{}\". Valid options are \"error\", \"errorflow\" and \"all\".".format(option))


    def write_prosumer_err2excel(self, agg_day, prosumer, outputfile, option = "error"):
        """
        This method writes the error dictionary for a specific prosumer within a specific number of aggregationd days to the excel-file given.

        Args:
            agg_day: Number of days used for the aggregation.
            prosumer: Specific prosumer for which the errors shall be written.
            outputfile: Path to the excel-file to write the errors to (.xlsx or .xlsm).
            option: Decides which error dictionary shall be written to excel.
            - possible values: "error" (__errors is written), "errorflow" (__errorsflow is written), "all" (both are written)
            - default: "error"
        """
        if option == "error":
            if agg_day in self.__errors.keys() and prosumer in self.__errors[agg_day].keys():
                err_dict = self.__errors
                one_dict = True
            else:
                print("\033[0;31mError:\033[0;0m Unvalid option. Error is not calculated for the aggregation day \"{}\" and prosumer \"{}\" given.".format(agg_day,prosumer))
                return
        elif option == "errorflow":
            if agg_day in self.__errorsflow.keys() and prosumer in self.__errorsflow[agg_day].keys():
                err_dict = self.__errorsflow
                one_dict = True
            else:
                print("\033[0;31mError:\033[0;0m Unvalid option. Errorflow is not calculated for the aggregation day \"{}\" and prosumer \"{}\" given.".format(agg_day,prosumer))
                return
        elif option == "all":
            option_valid = self.__verify_option_all(agg_day, prosumer)
            if not option_valid:
                print("\033[0;31mError:\033[0;0m Unvalid option. Not both errors, error and errorflow, were calculated for the aggregation day \"{}\" and prosumer \"{}\" given.".format(agg_day,prosumer))
                return

            # If incorrect keys were provided, __errors and __errorsflow were calculated for different rsl.
            # Calculate which option is valid for each rsl.
            keys = self.__get_rsl_options(agg_day,prosumer)
            one_dict = False
        else:
            print("\033[0;31mError:\033[0;0m Unvalid option \"{}\". Valid options are \"error\", \"errorflow\" and \"all\".".format(option))
            return

        # Test if the ending of the outputfile is correct.
        if not (os.path.splitext(outputfile)[-1] in ('.xlsm', '.xlsx')):
            print("\033[0;31mError:\033[0;0m The file extension of the outputfile should be either .xlsx or .xlsm.")
            return

        # Test if file exists; if not, save an empty workbook
        if not os.path.isfile(outputfile):
            wb = openpyxl.Workbook()
            wb.save(outputfile)

        # Write the errors to the file by using openpyxl and pandas excelwriter without overwriting the content of 'Sheet'.
        wb = openpyxl.load_workbook(outputfile)
        if not ('Sheet' in wb.sheetnames):                                  # Create 'Sheet' if it does not exist yet
            wb.create_sheet('Sheet')

        with pd.ExcelWriter(outputfile, mode="a", engine="openpyxl", if_sheet_exists="overlay") as writer:
            writer.book = wb
            writer.sheets = dict((ws.title, ws) for ws in wb.worksheets)    # Save what is already in the workbook.
            ws = wb['Sheet']                                                # New values will be added to 'Sheet'.
            if ws.max_row == 1:                                             # Use offset to prevent overwriting content of 'Sheet'.
                startrow = ws.max_row   # if Sheet is empty, start writing in line 1
            else:
                startrow = ws.max_row+1 # if not empty, do not overwrite last line
            ws.cell(row=startrow, column=1, value=agg_day)                  # agg_day is written in the first column.
            j=1
            ws.cell(row=startrow+j, column=2, value="prosumer: "+str(prosumer)) # prosumer is written in the second column.
            j+=1

            if one_dict:
                for rsl in err_dict[agg_day][prosumer].keys():
                    ws.cell(row=startrow+j, column=3, value="rsl: "+str(rsl))   # Write number of rsl in third column.
                    df = pd.DataFrame.from_dict(err_dict[agg_day][prosumer][rsl], orient='index')   # Convert errors to pd.DataFrame
                    df.to_excel(writer, sheet_name='Sheet', startrow=startrow+j, startcol=3)        # Write errors to the Sheet starting in 4. column.
                    j += len(df.index)+3
            else:
                # Write __errors and __errorsflow for each rsl (depending if both or only one is available)
                for rsl in keys.keys():
                    if keys[rsl] == "all":
                        ws.cell(row=startrow+j, column=3, value="rsl: "+str(rsl))                               # Write number of rsl in third column
                        df1 = pd.DataFrame.from_dict(self.__errors[agg_day][prosumer][rsl], orient='index')     # Convert errors to pd.DataFrame
                        df2 = pd.DataFrame.from_dict(self.__errorsflow[agg_day][prosumer][rsl], orient='index')
                        df1.to_excel(writer, sheet_name='Sheet', startrow=startrow+j, startcol=3)               # Write errors to the Sheet starting in 4. column
                        j+= len(df1.index)+2
                        df2.to_excel(writer, sheet_name='Sheet', startrow=startrow+j, startcol=3)
                        j += len(df2.index)+3
                    elif keys[rsl] == "errorflow":
                        ws.cell(row=startrow+j, column=3, value="rsl: "+str(rsl))                               # Write number of rsl in third column
                        df = pd.DataFrame.from_dict(self.__errorsflow[agg_day][prosumer][rsl], orient='index')  # Convert errors to pd.DataFrame
                        df.to_excel(writer, sheet_name='Sheet', startrow=startrow+j, startcol=3)                # Write errors to the Sheet starting in 4. column
                        j += len(df.index)+3
                    elif keys[rsl] == "error":
                        ws.cell(row=startrow+j, column=3, value="rsl: "+str(rsl))                           # Write number of rsl in third column
                        df = pd.DataFrame.from_dict(self.__errors[agg_day][prosumer][rsl], orient='index')  # convert errors to pd.DataFrame
                        df.to_excel(writer, sheet_name='Sheet', startrow=startrow+j, startcol=3)            # Write errors to the Sheet starting in 4. column
                        j += len(df.index)+3

    def get_prosumer_keys_dict(self):
        """
        This method returns the dictionary containing the lists of optimization keys for each prosumer (__keys_dict).

        Returns:
            __keys_dict: Dictionary containing the lists of optimization keys for each prosumer.
        """
        return self.__keys_dict

    def set_prosumer_keys(self, prosumer, key_list):
        """
        This method changes the list of optimization keys for a specific prosumer.

        Args:
            prosumer: Name of the prosumer for whom the list of keys shall be changed.
            key_list: The new list of keys for the prosumer.
        """
        self.__keys_dict[prosumer] = key_list


    def write_extrapolated_agg_sol2excel(self, agg_day, outputfile="output_files/res_agg_0.xlsx"):
        """
        This method writes the saved extrapolated solution for a specific number of aggregation days to the excel-file given.
        The different values for the prosumers are written to different sheets with the sheetname indicating the prosumer and rsl.

        Args:
            agg_day: Number of aggregation days
            outputfile: Path to the excel-file to write the solution to (.xlsx or .xlsm).
                - default: output_files/res_agg_0.xlsx
        """

        if agg_day not in self.__extrapolated_agg_sol.keys():
            print("\033[0;31mError:\033[0;0m No extrapolated solution was calculated for the number of aggregation days \"{}\" provided".format(agg_day))
            return

        # Test if the ending of the outputfile is correct.
        if not (os.path.splitext(outputfile)[-1] in ('.xlsm', '.xlsx')):
            print("\033[0;31mError:\033[0;0m The file extension of the outputfile should be either .xlsx or .xlsm.")
            return

        # Test if file exists; if not, create empty workbook, otherwise open existing one
        if not os.path.isfile(outputfile):
            wb = openpyxl.Workbook()
            wb.save(outputfile)
            wb = openpyxl.load_workbook(outputfile)
            del wb['Sheet']                             # Sheet is empty and not needed, therefore delete it
        else:
            wb = openpyxl.load_workbook(outputfile)

        # Write the extrapolated solutions to the file by using openpyxl and pandas excelwriter without overwriting the content of 'Sheet'.
        for prosumer in self.__extrapolated_agg_sol[agg_day].keys():
            for rsl in self.__extrapolated_agg_sol[agg_day][prosumer].keys():
                if not ('{}_{}'.format(prosumer,rsl) in wb.sheetnames):             # Create sheet 'prosumer_rsl', if it does not exist yet
                    wb.create_sheet('{}_{}'.format(prosumer,rsl))
                with pd.ExcelWriter(outputfile, mode="a", engine="openpyxl", if_sheet_exists="overlay") as writer:
                    writer.book = wb
                    writer.sheets = dict((ws.title, ws) for ws in wb.worksheets)    # Save what is already in the workbook.
                    ws = wb['{}_{}'.format(prosumer,rsl)]
                    if ws.max_row == 1:                                             # Use offset to prevent overwriting content of the sheet.
                        startrow = 0            # if Sheet is empty, start writing in line 1
                    else:
                        startrow = ws.max_row+1 # if not empty, do not overwrite last line
                    self.__extrapolated_agg_sol[agg_day][prosumer][rsl].to_excel(writer, sheet_name='{}_{}'.format(prosumer,rsl), startrow=startrow)


    def write_extrapolated_sol2excel(self):
        """
        This method writes all saved extrapolated solutions from the aggregated models to excel-files.
        For each scenario/number of aggregation days, a new excel-file is created
        with the name "res_agg_{}" ({} indicates the number of aggregation days).
        The files can be found in the folder "output_files".
        """
        # If folder doesn't exist, then create it.
        bool_folder = os.path.isdir("output_files")
        if not bool_folder:
            print("\033[0;33mWarning:\033[0;0m Folder for outputs does not exist.")
            print("Creating folder \"output_files\"...")
            os.makedirs("output_files")
            print("Continue.")

        for agg_day in self.__extrapolated_agg_sol.keys():
            self.write_extrapolated_agg_sol2excel(agg_day, "output_files/res_agg_{}.xlsx".format(agg_day))

    #######################################
    # private:

    def __find_invalid_keys(self, df, prosumer):
        """
        This method verifies wether the keys for the prosumer, given in __keys_dict[prosumer], are contained in the keys of the data frame given
        and returns all invalid keys contained in __keys_dict[prosumer].

        Args:
            df: Data frame in which the keys are searched for.
            prosumer: Name of the prosumer to find the corresponding keys in __keys_dict.

        Returns:
            List containing all invalid keys of __keys_dict[prosumer] (is empty if all keys are valid).
        """
        df_keylist = list(df.keys())
        invalid_keys = [elem for elem in self.__keys_dict[prosumer] if elem not in df_keylist]
        return invalid_keys

    def __find_invalid_prosumer(self, prosumer_list, reference):
        """
        This method verifies wether the list of prosumer provided is contained within the reference scenario
        and returns all invalid prosumers contained in the list.

        Args:
            prosumer_list: List of prosumers.
            reference: Instance of the Main class as reference scenario.

        Returns:
            List containing all invalid prosumers of prosumer_list (is empty if all prosumers are valid).
        """
        invalid_keys = [elem for elem in prosumer_list if elem not in reference.prosumer.keys()]
        return invalid_keys

    def __verify_option_all(self, agg_day, prosumer=None):
        """
        Checks if errors and errorflows both were calculated for the number of aggregation days provided.
        Additionally, if a prosumer name is provided, it is checked if both errors were calculated
        for the prosumer within the number of aggregation days provided.

        Args:
            agg_day: The number of aggregation days.
            prosumer (optional): String indicating the prosumer for the aggregation day.

        Returns:
            Boolean:
                True - if both errory types were calculated for the number of aggregation days (and prosumer) provided.
                False - in all other cases.
        """
        if prosumer is not None:
            if agg_day in self.__errors.keys() and agg_day in self.__errorsflow.keys():
                if prosumer in self.__errors[agg_day].keys() and prosumer in self.__errorsflow[agg_day].keys():
                    return True
                else:
                    return False
            else:
                return False
        else:
            if agg_day in self.__errors.keys() and agg_day in self.__errorsflow.keys():
                return True
            else:
                return False

    def __get_agg_day_options(self):
        """
        Returns a dictionary of all aggregation days for which errors were calculated.
        The values indicate which option for the corresponding print function (write_aggday_err2excel) may be chosen
        to write each aggregation day to an excel-file.
        """
        keys = {}
        for key in self.__errors.keys():
            keys[key] = "error"

        for key in self.__errorsflow.keys():
            if key in keys.keys():
                keys[key] = "all"
            else:
                keys[key] = "errorflow"

        return keys

    def __get_prosumer_options(self, agg_day):
        """
        Returns a dictionary of all prosumers for which errors were calculated for the number of aggregation days given.
        The values indicate which option may be chosen to write the prosumer
        for this number of aggregation days to an excel-file (write_prosumer_err2excel).

        Args:
            agg_day: Number of aggregation days as integer for which the prosumer options shall be returned for.

        Note:
            This function is used internally such that it was checked that both errors, error and errorsflow,
            were calculated for the number of aggregation days given before calling this function.
        """
        keys = {}
        for key in self.__errors[agg_day].keys():
            keys[key] = "error"

        for key in self.__errorsflow[agg_day].keys():
            if key in keys.keys():
                keys[key] = "all"
            else:
                keys[key] = "errorflow"

        return keys

    def __get_rsl_options(self, agg_day, prosumer):
        """
        Returns a dictionary of all rsl for which errors were calculated for the number of aggregation days and prosumer given.
        The values indicate which option may be chosen to write the rsl for this prosumer and number of aggregation days to an excel-file.

        Args:
            agg_day: Number of aggregation days as integer for which the rsl options shall be returned.
            prosumer: Prosumer for which the rsl options shall be returned.

        Note:
            This function is used internally such that it was checked that both errors, error and errorsflow,
            were calculated for the number of aggregation days and prosumer given before calling this function.
        """
        keys = {}
        for key in self.__errors[agg_day][prosumer].keys():
            keys[key] = "error"

        for key in self.__errorsflow[agg_day][prosumer].keys():
            if key in keys.keys():
                keys[key] = "all"
            else:
                keys[key] = "errorflow"

        return keys