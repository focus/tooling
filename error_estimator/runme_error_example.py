"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
# !! THIS SCRIPT SHOULD BE RUN FROM THE FRAMEWORK'S ROOT DIRECTORY !!
#
# runme_error_example.py is an example script how to use the ErrorEstimator class when using temporal aggregation.
# The main functionalities are as follows:
# 1. calculate errors and error_flows for the scenario given.
# 2. print the calculated values to excel-files.


########## Import ErrorEstimator class for temporal aggregation
from tools.error_estimator import ErrorEstimator
###############################################################


########## Imports for Main class #############################
# Importing the necessary files
# from scripts import extract_inputs
import numpy as np
import scripts
import prosumer_library.prosumer_models as prosumer_models
import scripts.Database.use_database as use_db
import time
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import os

import scripts.time_series_processing as ts
import scripts.calc_irradiance as irr
from temporal_aggregator import aggregation_module
###############################################################


########## Main class is copied from runme_db.py (for usage refer to the corresponding documentation)
class Main:
    def __init__(self, mode, *args, aggregation=False, num=8):
        # The number of elements in prosumer name defines the number of prosumers
        # self.prosumer stores all instantiated prosumers in a dict
        # self.prosumer = {prosumer_name: prosumer obj}
        self.prosumer = {}
        self.num = num

        # The Process can only run with one prosumer now.
        # In the next steps, multiple prosumers will be allowed
        if mode == 1:  # connect with data bank
            # todo: adjust use_database get_scenario after the adjustment for
            #  mode 2
            start_db = time.perf_counter()
            end_db = time.perf_counter()

            # create sector matrices for each prosumer
            scenario = args[0]
            sector_matrices, input_profiles, prosumer_config, scenario_time, comp_dict, name_list = \
                use_db.get_scenario(scenario)

            # Set the simulation time frame
            t_st = datetime.strptime(scenario_time['t_start'][0], "%Y-%m-%dT%H:%M:%S")  # needs to be string
            t_end = datetime.strptime(scenario_time['t_end'][0], "%Y-%m-%dT%H:%M:%S")  # "2019-07-25 00:00:00"
            t_diff = t_end - t_st
            self.t_start = str(t_st)
            self.t_horizon = t_diff.days * 24  # time in [h]
            self.t_step = scenario_time['t_step'][0]  # time step in [h]
        elif mode == 2:  # use local data
            start_db = 0
            end_db = 0
            # Set the simulation time frame
            self.t_start = args[1]  # "2019-07-25 00:00:00"
            self.t_horizon = args[2]  # time in [h]
            self.t_step = args[3]  # time step in [h]

            # Extraction of input files
            step = args[3]
            name_list = args[0].keys()
            input_profiles = dict.fromkeys(name_list)
            prosumer_config = dict.fromkeys(name_list, {})

            for each_prosumer in name_list:
                topology_path = args[0][each_prosumer]['topology_path']
                configuration_path = args[0][each_prosumer]['config_path']
                input_profiles[each_prosumer], sector_matrices = scripts.extract_inputs.run(topology_path, step)
                prosumer_config[each_prosumer] = scripts.extract_inputs.get_config(configuration_path)
                self.prosumer[each_prosumer] = prosumer_models.BaseProsumer(
                    name=each_prosumer,
                    prosumer_configuration=prosumer_config[each_prosumer])

                # Set components from matrix
                self.prosumer[each_prosumer].set_components(sector_matrices)
                global_irradiance = input_profiles[each_prosumer][
                    'irradiance_BSRN']
                temperature = input_profiles[each_prosumer]['temperature']
                charging_profile = input_profiles[each_prosumer][
                    'charging_profile_home_ID.3']
                # 3 part irradiance from BSRN for use with calc_irradiance
                irradiance_components = input_profiles[each_prosumer][
                    'irradiance_components']

                self.prosumer[each_prosumer].add_strategy(
                    ['variable_operation_costs', 'own_consumption', 'annuity',
                     'co2'])
                print('The strategies added are: ' + str(
                    self.prosumer[each_prosumer].get_strategy()))

                # Set dates for optimization problem
                self.prosumer[each_prosumer].set_dates(self.t_start,
                                                      self.t_horizon,
                                                      t_step=self.t_step)

                profiles1 = ts.generate_profile(t_start, t_step, t_horizon,
                                                generate_elec_demand=(
                                                'g1', 32905),
                                                generate_therm_demand=(
                                                'GBD', 115154, temperature),
                                                generate_water_demand=(
                                                'GBD', 11882, temperature),
                                                charging_power=charging_profile,
                                                prediction='Perfect')
                # profiles1 = ts.generate_profile(t_start, t_step, t_horizon,
                #                                 generate_elec_demand=('h0', 4006),
                #                                 generate_therm_demand=(
                #                                 'EFH', 10551, temperature),
                #                                 generate_water_demand=(
                #                                 'EFH', 1816, temperature),
                #                                 charging_power=charging_profile,
                #                                 prediction='Perfect')

                profiles2 = ts.generate_profile(t_start, t_step, t_horizon,
                                                # irradiance=global_irradiance,
                                                air_temperature=temperature)

                # self.prosumer[each_prosumer].add_profile(profiles1)
                # self.prosumer[each_prosumer].add_profile(profiles2)

                ###
                # Use the calc_irradiance tool to calculate the total irradiance under consideration of tilt, orientation, etc
                # Uses 'Lindenberg2006BSRN_Irradiance_60sec.csv' which results in a yearly PV-yield of 779.43 kWh/kWp
                lambda_1 = 14.122
                lambda_st = 15
                phi = 52.21
                psi_f = 0
                beta = 30

                g_t = irr.generate_g_t_series(irradiance_components, beta,
                                              psi_f, phi, lambda_st, lambda_1,
                                              self.t_start,
                                              self.t_horizon, t_step=1)
                # self.prosumer[each_prosumer].add_profile({'irradiance': g_t})

                if aggregation == True: # Aggregate profiles before adding them to prosumer
                    print("Started with (!!!) aggregation!")

                    ###
                    # Import aggregation module and define aggregation parameters
                    import temporal_aggregator.aggregation_module as am
                    cluster_method = 'hierarchical'
                    extreme_period_method = "None"#"replace_cluster_center"
                    # Note: if 24/hoursPerPeriod has more than 2 digits after the decimal point errors may occur
                    hoursPerPeriod = 24
                    # Create aggregator object
                    self.aggregator = am.Aggregator({**profiles1, **profiles2, **{'irradiance': g_t}}, clusterMethods=[cluster_method])
                    # peakMin_list = aggregator.getInputKeys()
                    # peakMax_list = aggregator.getInputKeys()
                    peakMin_list = []
                    peakMax_list = []
                    meanMin_list = []
                    meanMax_list = []
                    # Aggregate time series
                    self.aggregator.aggregate(self.num, hoursPerPeriod, _extremePeriodMethod=extreme_period_method, _peakMin=peakMin_list, \
                        _peakMax=peakMax_list, _meanMin=meanMin_list, _meanMax=meanMax_list)
                    # Read aggregated time series
                    aggregated_profiles = self.aggregator.getAggregationsDict()[cluster_method]

                    self.periodOrder = self.aggregator.getAggregations()[cluster_method]['agg_object'].clusterOrder
                    self.hoursPerPeriod = hoursPerPeriod

                    # Adapt time horizon
                    index = list(list(aggregated_profiles.values())[0].index)
                    self.t_start = index[0].strftime("%Y-%m-%d %H:%M:%S")
                    self.t_horizon = len(index)*24/hoursPerPeriod  # time in [h]
                    self.t_step = 24/hoursPerPeriod  # time step in [h]

                    self.prosumer[each_prosumer].set_dates(self.t_start, self.t_horizon, t_step=self.t_step)

                    # Add aggregated profiles to prosumer
                    self.prosumer[each_prosumer].add_profile(aggregated_profiles)

                else: # No aggregation wished - use profiles as created
                    self.prosumer[each_prosumer].add_profile(profiles1)
                    self.prosumer[each_prosumer].add_profile(profiles2)
                    self.prosumer[each_prosumer].add_profile({'irradiance': g_t})

        else:  # return error
            raise ValueError('Mode can only be 1 or 2.')
        self.connect_with_db = end_db - start_db
        self.prosumer_name_list = name_list  # save prosumer names in a list

        # Set components from matrix
        if mode == 1:
            self.prosumer[name_list[0]].set_components(sector_matrices[name_list[0]], comp_dict[name_list[0]])
            global_irradiance, temperature = use_db.config_profiles(input_profiles[name_list[0]], self.t_step)

    def run_optimization(self, name_list):
        # ToDo: callback, abbruchbedingung, maybe check the feasebility of the model before building,
        #  häufige fehler bei der eingabe prüfen usw
        for each_prosumer in name_list:
            self.prosumer[each_prosumer].run_optimization(strategy_name=['annuity'],
                                                          solver_name='gurobi',
                                                          commentary=True)

    def show_results(self, name_list):
        if not os.path.exists('output_files'):
            os.makedirs('output_files')
        # Results
        for each_prosumer in name_list:
            t1 = time.time()
            self.prosumer[each_prosumer].show_yourself()['_BaseProsumer__model'].write(
                'output_files/model.lp', io_options={'symbolic_solver_labels': True})
            t2 = time.time()
            print('show_yourself took [s]:\t' + str(t2 - t1))

            # ToDo: wrap errors, exceptions in all functions
            # Results df
            t3 = time.time()
            results = self.prosumer[each_prosumer].get_results()
            t4 = time.time()
            print('get_results took [s]:\t' + str(t4 - t3))
            for rsl in results:
                t5 = time.time()
                results[rsl].to_pickle('output_files/results_' + str(rsl) + '.pkl')
                # todo: make it one excel file with several pages
                with pd.ExcelWriter('output_files/results_' + str(rsl) + '.xlsx') as writer:
                    results[rsl].to_excel(writer)
                t6 = time.time()
                print('write_excel took [s]:\t' + str(t6 - t5))

            print(self.prosumer[each_prosumer].get_payoff_table())
            print(self.prosumer[each_prosumer].get_pareto_values())

            self.prosumer[each_prosumer].plt_pareto_front(dpi=100)
################################################################

########## Additionals##########################################
# timeit is not required for the usage of the ErrorEstimator class.
# It is used here to observe the timings of the different steps from creating the Main class instances to calculating the errors.
def timeit(func):
    tic = time.time()
    _ret = func() # call passed function
    toc = time.time()
    return (toc-tic), _ret

# This argument can be used to extend the name of the outputfiles to differentiate between different iterations of running this script
import argparse
parser = argparse.ArgumentParser(description='Start optimization from DB or local data with ')
parser.add_argument('-i', type=int, action="store", dest="iter", default=0)
options = parser.parse_args()
#################################################################


########## Initialize Scenario #################################
# initialization with local data
data_source = 2  # [1]: datasource from data bank; [2]: datasource from local
scenario_path = 'input_files/office_pv_heatpump_charge'  # heatgrid_exchanger_simp'
config_path = 'prosumer_library/database/config_base.csv'
prosumer_dict = {'office': {'topology_path': scenario_path, 'config_path': config_path}}
# Set the simulation time frame
t_start = "2019-01-01 00:00:00"  # "2019-07-25 00:00:00"
t_horizon = 24 * 365  # time in [h]
t_step = 60/60  # time step in [h]
#################################################################


########## Calculate reference scenario as instance of Main class
t_prep_ref, main = timeit(lambda: Main(data_source, prosumer_dict, t_start, t_horizon, t_step, aggregation=False))
t_opt_ref, _ret = timeit(lambda: main.run_optimization(main.prosumer_name_list))
#################################################################


########## Initiate ErrorEstimator ##############################
# here: main.prosumer_name_list is used to find the correct name of the prosumer in this scenario.
# Note: This scenario only has one prosumer
# The prosumers are the keys of the dict, the values are a list of the optimized parameters for the specific prosumer in this scenario.
# Note: The tuples are the same as the ones in the result excel-file of the Main class.
err_estimator = ErrorEstimator({list(main.prosumer_name_list)[0]: [('power', 'pv_roof'), ('power', 'inv_pv_bat'), ('cap', 'battery'), ('power', 'heat_pump'), ('cap', 'water_tes')]})
#################################################################


########## Initialize the aggregation models and use ErrorEstimator to calculate the errors
import sys
for num in [16, 8, 32]: #these aggregation days are tested

    ########## Initialize aggregated scenario and optimize it
    # Note: Additionally, the aggregation error before optimization is calculated
    t_prep_agg, aggregated = timeit(lambda: Main(data_source, prosumer_dict, t_start, t_horizon, t_step, aggregation=True, num=num))
    input_error = aggregated.aggregator.getErrors()
    input_error_rmse = input_error["hierarchical"].iloc[1] # RMSE always first row of returned DF
    t_opt_agg, _ref = timeit(lambda: aggregated.run_optimization(aggregated.prosumer_name_list))
    ##########################################################

    ########## Calculate error and errorflows for the optimized scenario using the ErrorEstimator class
    # All calculated errors and error_flows are saved internally in the ErrorEstimator class instance.
    # Here: For some aggregation days only error or error_flow is calculated, for other aggregation days both.
    # main is the reference scenario, aggregated the aggregated scenario.
    # main.prosumer_name_list is used to give an accurate list of the prosumers to calculate the errors for.
    # num refers to the number of aggregated days in the aggregated scenario.
    tic = time.time()
    if num == 8:
        err_values = err_estimator.error(main, aggregated, main.prosumer_name_list, num)
    elif num == 32:
        errflow_values = err_estimator.error_flow(main, aggregated, main.prosumer_name_list, num)
    else:
        err_values = err_estimator.error(main, aggregated, main.prosumer_name_list, num)
        errflow_values = err_estimator.error_flow(main, aggregated, main.prosumer_name_list, num)
    t_post = time.time() - tic
    ##########################################################

    ########## Extrapolate the aggregated solution to the length of the reference scenario
    # All extrapolated solutions are saved internally in the ErrorEstimator class instance.
    agg_completr = err_estimator.extrapolate_agg_sol(main, aggregated, main.prosumer_name_list, num)
    ##########################################################

    ########### Timings are written to txt-file (not part of ErrorEstimator class)
    original_stdout = sys.stdout # Save a reference to the original standard output
    with open('output_files/time_{}_{}.txt'.format(num,options.iter), 'w') as f:
        sys.stdout = f # Change the standard output to the file we created.
        print("============ Execution Times for n = {}=============".format(num))
        print("\t\t\t = Reference = \t = Aggregated =")
        print("Pre-processing [s]: \t\t{:.3f}\t\t{:.3f}".format(t_prep_ref, t_prep_agg))
        print("(Interaction with database [s]:\t{})".format(main.connect_with_db))
        print("Optimization [s]: \t\t{:.3f}\t\t{:.3f}".format(t_opt_ref, t_opt_agg))
        print("Post-processing [s]: \t\t{:.3f}".format(t_post))
        print("----------------------------------------")
        print("Total [s]: \t\t{:.3f}\t\t{:.3f}".format(t_prep_ref+t_opt_ref, t_prep_agg+t_opt_agg))
        print("==========================================")
        sys.stdout = original_stdout # Reset the standard output to its original value
    ###########################################################


######### Write the calculated errors of the ErrorEstimator in excel-files
# Two errors can be calculated: error and error_flows.
# Options for all print-functions are "error", "errorflow" and "all" to print only one error type or both.
# If no option is given only "error" is saved.
# Note: If the dictionaries are empty, i.e. no errors were calculated, no file may be created.
# Note: If the files already exist, no content is overwritten.

# This print-function prints the values for all aggregations days and prosumers.
err_estimator.write_err2excel("output_files/output_test1_{}.xlsx".format(options.iter))
err_estimator.write_err2excel("output_files/output_test2_{}.xlsx".format(options.iter), "errorflow")
err_estimator.write_err2excel("output_files/output_test3_{}.xlsx".format(options.iter), "all")

# If an existing file is chosen, the data is appended to the sheet "Sheet" of the excel-file.
err_estimator.write_err2excel("output_files/output_test1_{}.xlsx".format(options.iter))

# This print-function prints the values for a specific aggregation day but all prosumers within this day.
# Note: If an invalid option is chosen (i.e. the chosen error is not calculated for this aggregation day),
#       no excel-file is created but an error message is printed.
err_estimator.write_aggday_err2excel(16, "output_files/output_test4_{}.xlsx".format(options.iter), "all")        #should work
err_estimator.write_aggday_err2excel(32, "output_files/output_test5_{}.xlsx".format(options.iter), "error")      #should not work

# This print-function prints the values for a specific aggregation day and one specific prosumer within this day.
# Note: If an invalid option is chosen (i.e. the chosen error is not calculated for this aggregation day or prosumer),
#       no excel-file is created but an error message is printed.
err_estimator.write_prosumer_err2excel(16, "office", "output_files/output_test6_{}.xlsx".format(options.iter))               #should work
err_estimator.write_prosumer_err2excel(16, "prosumer2", "output_files/output_test7_{}.xlsx".format(options.iter), "all")     #should not work
err_estimator.write_prosumer_err2excel(8, "office","output_files/output_test8_{}.xlsx".format(options.iter), "errorflow")    #should not work
###############################################################

########## Write the extrapolated solutions of the ErrorEstimator to excel-files
# Note: If no extrapolations were calculated, no files are created.
# Note: If the excel-files already exist, no content is overwritten.

# This function writes all saved extrapolated solutions to excel-files (one file for each number of aggregation days).
err_estimator.write_extrapolated_sol2excel()

# This function writes the extrapolated solution of a specific number of aggregation days to the outputfile provided.
err_estimator.write_extrapolated_agg_sol2excel(8, "output_files/res_agg_0.xlsx")
###############################################################
