"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import pytest
from .ErrorEstimator import ErrorEstimator
from unittest.mock import Mock, MagicMock
import pandas as pd
import numpy as np
import os

#This function returns a mocked dictionary based on a real dictionary d
def mock_results(d):
    dict_mock = MagicMock()
    dict_mock.__getitem__.side_effect = d.__getitem__
    dict_mock.__iter__.side_effect = d.__iter__
    return dict_mock

#Fixture which creates an instance of the aggregator for the class
#Note: There is one instance for all tests using this fixture -> reset instance between tests
@pytest.fixture(scope="class")
def error_estimator():
    return ErrorEstimator({"office": [('power', 'pv_roof'), ('power', 'inv_pv_bat'), ('cap', 'battery'), ('power', 'heat_pump'), ('cap', 'water_tes')]})


#Class to test the ErrorEstimator Module
@pytest.mark.ErrorEstimator
class TestClass_ErrorEstimator:

    ##########init_test#########
    @pytest.mark.init_func
    def test_init(self):
        err_estimator = ErrorEstimator({"office": [('power', 'pv_roof'), ('power', 'inv_pv_bat'), ('cap', 'battery'), ('power', 'heat_pump'), ('cap', 'water_tes')]})
        assert isinstance(err_estimator, ErrorEstimator)

    ##########test get-functions##########
    @pytest.mark.getter
    def test_get_prosumer_keys_dict(self, error_estimator):
        assert isinstance(error_estimator.get_prosumer_keys_dict(), dict)
        assert list(error_estimator.get_prosumer_keys_dict().keys()) == ["office"]
        assert error_estimator.get_prosumer_keys_dict()["office"] == [('power', 'pv_roof'), ('power', 'inv_pv_bat'), ('cap', 'battery'), ('power', 'heat_pump'), ('cap', 'water_tes')]

    ##########test set-functions##########
    @pytest.mark.setter
    def test_set_prosumer_keys(self, error_estimator):
        error_estimator.set_prosumer_keys("office", [('cap', 'pump')])
        assert isinstance(error_estimator.get_prosumer_keys_dict(), dict)
        assert list(error_estimator.get_prosumer_keys_dict().keys()) == ["office"]
        assert error_estimator.get_prosumer_keys_dict()["office"] == [('cap', 'pump')]

    ##########test functionality##########
    @pytest.mark.functionality
    def test_error(self, error_estimator):
        #the function error() which shall be tested requires following as arguments: 2 objects of class Main (referece, aggregated), list, int-number
        #class objects are mocked in the following

        #create prosumer list, and integer value for the function call which shall be tested
        prosumer_name_list = ['office']
        agg_days = 3

        # The instances of the Main class are already optimized -> results must be mocked
        # Mock-Results from Optimization
        df = pd.DataFrame({'d1': [5,5,5], 'd2': [2,2,2]})
        df2 = pd.DataFrame({'d1': [4,4,4], 'd2': [1,1,1]})
        d = {0: df}                     #results for reference object
        d2 = {0: df2}                   #results for aggregated object
        dict_mock = mock_results(d)
        dict2_mock = mock_results(d2)

        # error-estimator is temporally set to compare the mocked optimization parameters
        # adapt prosumer_keys to mock-results
        keys = error_estimator.get_prosumer_keys_dict()
        error_estimator.set_prosumer_keys('office', ['d1', 'd2'])
        #keys_ver = error_estimator.get_prosumer_keys_dict()

        # create Mock Objects of the Main class with the mock results and other class-funcs which are needed in the function call to test
        reference = MagicMock()
        reference.prosumer.__getitem__.return_value.get_results.return_value = dict_mock    #mock reference.prosumer[eachprosumer].get_results()
        reference.prosumer.keys.return_value = ['office']                                   #mock reference.prosumer.keys()
        aggregated = MagicMock()
        aggregated.prosumer.__getitem__.return_value.get_results.return_value = dict2_mock  #mock aggregated.prosumer[eachprosumer].get_results()

        # call test-func with Mock objects
        error = error_estimator.error(reference, aggregated, prosumer_name_list, agg_days)

        # reset error-estimator to original optimization parameters
        error_estimator.set_prosumer_keys('office', keys)

        # verify that the function call error() returned the expected values
        verify_dict = {'office': {0: {'rmse': {'d1': 1, 'd2': 1, 'total': 1.0}, 'mae': {'d1': 1, 'd2': 1, 'total': 1.0}, 'rel': {'d1': -0.2, 'd2': -0.5, 'total': np.NaN}}}}
        assert error == verify_dict


    @pytest.mark.functionality
    def test_error_flow(self, error_estimator):
        #the function error_flow() which shall be tested requires following as arguments: 2 objects of class Main (referece, aggregated), list, int-number
        #class objects are mocked in the following

        #create prosumer list, and integer value for the function call which shall be tested
        prosumer_name_list = ['office']
        agg_days = 3

        # The instances of the Main class are already optimized -> results must be mocked
        # Mock-Results from Optimization
        df = pd.DataFrame({'d1': [5,5,5], 'd2': [2,2,2]})
        df2 = pd.DataFrame({'d1': [4,4,4], 'd2': [1,1,1]})
        d = {0: df}                     #results for reference object
        d2 = {0: df2}                   #results for aggregated object
        dict_mock = mock_results(d)
        dict2_mock = mock_results(d2)

        # create Mock Objects of the Main class with all class functions and attributes used in the function to test
        reference = MagicMock()
        reference.prosumer.__getitem__.return_value.get_results.return_value = dict_mock        #mock reference.prosumer[eachprosumer].get_results()
        reference.prosumer.keys.return_value = ['office']                                       #mock reference.prosumer.keys()
        aggregated = MagicMock(hoursPerPeriod=1, periodOrder=[0,1,2])                           #mock aggregated.hoursPeriod, aggregated.periodOrder
        aggregated.prosumer.__getitem__.return_value.get_results.return_value = dict2_mock      #mock aggregated.prosumer[eachprosumer].get_results()

        # call test-func with Mock objects
        error = error_estimator.error_flow(reference, aggregated, prosumer_name_list, agg_days)

        # verify that the function call error_flow() returned the exptected values
        verify_dict = {'office': {0: {'rmse': {'d2': 1.0}, 'mae': {'d2': 1.0}}}}
        assert error == verify_dict

    @pytest.mark.functionality
    def test_extrapolate_agg_sol(self, error_estimator):
        #the function extrapolat_agg_sol() which shall be tested requires following as arguments: 2 objects of class Main (referece, aggregated), list, int-number
        #class objects are mocked in the following

        #create prosumer list, and integer value for the function call which shall be tested
        prosumer_name_list = ['office']
        agg_days = 3

        # The instances of the Main class are already optimized -> results must be mocked
        # Mock-Results from Optimization
        df = pd.DataFrame({'d1': [5,5,5,5], 'd2': [2,2,2,2]})
        df2 = pd.DataFrame({'d1': [4,5], 'd2': [1,2]})
        d = {0: df}                     #results for reference object
        d2 = {0: df2}                   #results for aggregated object
        dict_mock = mock_results(d)
        dict2_mock = mock_results(d2)


        # create Mock Objects of the Main class with all class functions and attributes used in the function to test
        reference = MagicMock()
        reference.prosumer.__getitem__.return_value.get_results.return_value = dict_mock    #mock reference.prosumer[eachprosumer].get_results()
        reference.prosumer.keys.return_value = ['office']                                   #mock reference.prosumer.keys()
        aggregated = MagicMock(hoursPerPeriod=2, periodOrder=[0,0])                         #mock aggregated.hoursPeriod, aggregated.periodOrder
        aggregated.prosumer.__getitem__.return_value.get_results.return_value = dict2_mock  #mock aggregated.prosumer[eachprosumer].get_results()

        # assert function call returns exptected values
        extrapolated = error_estimator.extrapolate_agg_sol(reference, aggregated, prosumer_name_list, agg_days)
        assert extrapolated['office'][0].equals(pd.DataFrame({'d1': [4,5,4,5], 'd2': [1,2,1,2]}))

    #########test output functions##########
    @pytest.mark.output
    @pytest.mark.parametrize(
    "test_input,expected",
    [("error", pd.DataFrame({'1': [0, 'rmse', 'mae', 'rel'], '2': ['d1', 1, 1, -0.2], '3': ['d2', 1, 1, -0.5], '4': ['total', 1, 1, 0]})),
    ("all", pd.DataFrame({'1': [0, 'rmse', 'mae', 'rel', 0, 0, 'mae', 'rmse'], '2': ['d1', 1, 1, -0.2, 0, 'd2', 1, 1], '3': ['d2', 1, 1, -0.5, 0, 0, 0, 0], '4': ['total', 1, 1, 0, 0, 0, 0, 0]})),
    ("errorflow", pd.DataFrame({'1': [0, 'mae', 'rmse'], '2': ['d2', 1, 1], '3': [0, 0, 0], '4': [0, 0, 0]}))],
)
    def test_write_err2excel(self, test_input, expected, error_estimator):
        #the function write_err2excel can be called with the options "error", "all", "errorflow". These are all tested one after the other through parameterization

        #prepare for test
        #no output-file should exist (otherwise comparison will not work)
        if(os.path.isfile("test_error_estimator_output.xlsx")):
            os.remove("test_error_estimator_output.xlsx")

        #create parameters for error calculation
        prosumer_name_list = ['office']
        agg_days = 3

        #Mock-Results for Optimization
        df = pd.DataFrame({'d1': [5,5,5], 'd2': [2,2,2]})
        df2 = pd.DataFrame({'d1': [4,4,4], 'd2': [1,1,1]})
        d = {0: df}
        d2 = {0: df2}
        dict_mock = mock_results(d)
        dict2_mock = mock_results(d2)

        #adapt prosumer_keys to fit to mock-results
        keys = error_estimator.get_prosumer_keys_dict()
        error_estimator.set_prosumer_keys('office', ['d1', 'd2'])

        #Mock Objects of Main-Class for function call
        reference = MagicMock()
        reference.prosumer.__getitem__.return_value.get_results.return_value = dict_mock
        reference.prosumer.keys.return_value = ['office']
        aggregated = MagicMock(hoursPerPeriod=1, periodOrder=[0,1,2])
        aggregated.prosumer.__getitem__.return_value.get_results.return_value = dict2_mock

        #create error-results saved in the error_estimator
        error_estimator.error(reference, aggregated, prosumer_name_list, agg_days)         # create error results
        error_estimator.error_flow(reference, aggregated, prosumer_name_list, agg_days)    # create error_flow results

        #return prosumer_keys to previous values
        error_estimator.set_prosumer_keys('office', keys)

        #execute function to test
        error_estimator.write_err2excel('test_error_estimator_output.xlsx', test_input)

        #verify file was created and verify contents
        assert os.path.isfile("test_error_estimator_output.xlsx")
        df_read = pd.read_excel("test_error_estimator_output.xlsx", usecols="D,E,F,G", skiprows=2, names=list("1234")).fillna(0)
        assert df_read.equals(expected)
        os.remove("test_error_estimator_output.xlsx") #only executes when assertion true

    @pytest.mark.output
    def test_write_extrapolated_sol2excel(self, error_estimator, capsys):

        if os.path.isfile("output_files/res_agg_3.xlsx"):
            with capsys.disabled():
                print("\n\033[0;33mWarning:\033[0;0m Test ErrorEstimator::test_write_extrapolated_sol2excel cannot be executed.")
                print("The file \"output_files/res_agg_3.xlsx\" does already exist.")
                print("Please delete this file before executing this test.")
            return

        #create parameters for function call
        prosumer_name_list = ['office']
        agg_days = 3

        #Mock-Results for Optimization
        df = pd.DataFrame({'d1': [5,5,5,5], 'd2': [2,2,2,2]})
        df2 = pd.DataFrame({'d1': [4,5], 'd2': [1,2]})
        d = {0: df}
        d2 = {0: df2}
        dict_mock = mock_results(d)
        dict2_mock = mock_results(d2)

        #Mock Objects of Main-Class for function call
        reference = MagicMock()
        reference.prosumer.__getitem__.return_value.get_results.return_value = dict_mock
        reference.prosumer.keys.return_value = ['office']
        aggregated = MagicMock(hoursPerPeriod=2, periodOrder=[0,0])
        aggregated.prosumer.__getitem__.return_value.get_results.return_value = dict2_mock

        #create extrapolated solution
        error_estimator.extrapolate_agg_sol(reference, aggregated, prosumer_name_list, agg_days)

        #execute function to test
        error_estimator.write_extrapolated_sol2excel()

        #verify file was created and verify content
        assert os.path.isfile("output_files/res_agg_3.xlsx")
        df_read = pd.read_excel("output_files/res_agg_3.xlsx", index_col=0).fillna(0)
        assert df_read.equals(pd.DataFrame({'d1': [4,5,4,5], 'd2': [1,2,1,2]}))
        os.remove("output_files/res_agg_3.xlsx") #only executes when assertion true

