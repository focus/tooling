# aggregation_module

InEEd-DC Optimization Framework wrapper for the _Time Series Aggregation Module (tsam)<sup>[1](#tsamfn)</sup>_, reducing the complexity and run-time of the prosumer optimization by reducing the amount of days considered. Time series can passed as file pahts, DataFrames or Dicts of DataFrames, which is the native way of representation in the InEEd-DC Prosumer Optimisation.

<a name="tsamfn"><sup>1</sup></a> [https://github.com/FZJ-IEK3-VSA/tsam](https://github.com/FZJ-IEK3-VSA/tsam)

## Requirements

- `pandas.DataFrame`
- `tsam`
  - Install via: `pip install tsam`
  - Further explanations found in the tsam git repository <sup>[1](#tsamfn)</sup>
- `tqdm`
- `pyplot`

## Usage / Example
1) Initialization
2) Aggregation
3) Aquire Results

__TBD__

## Documentation

### Initialization / Setter-Methods

`__init__(self, _input, clusterMethods=['k_means'])`
- Create Aggregator-Object with given input and the target cluster method
  - `_input : [String|pandas.DataFrame|Dict]` - Input time series given as file path, single dataframe or dict of series
  - `clusterMethods : [{'averaging', 'k_means', 'k_medoids', 'hierarchical'}]` - Define the methods used for determining aggregation cluster. Can hold _multiple methods_ at once!

`setClusterMethods(clusterMethods)`
- Change cluster method after initialization. Can hold _multiple methods_ at once!
  - `clusterMethods : [{'averaging', 'k_means', 'k_medoids', 'hierarchical'}]`

### Getter-Methods

`getClusterMethods()`
- Output currently used cluster Method

`getTS()`
- Return raw timeseries, i.e. the unaggregated input

`getInputKeys()`
- Return idtenfied keys of all inputted timeseries

`getAggregations()`
- Return aggregated timeseries

`getAggregationsDict()`
- Return aggregated timseries in a dict, mimicing the input dict

### Functionalities

`writeTS(args)`
- Write timeseries to file specified by `args`

`aggregate(_noTypical_periods = 8, _hoursPerPeriod = 24, _noSegments = 10, _extremePeriodMethod = "None",  _peakMin=[], _peakMax=[], _meanMin=[], _meanMax=[])`
- Aggregate given input time series
  - `_noTypical_periods : [Int]` - Number of resulting days in the aggregated timeseries
  - `_hoursPerPeriod " [Int]` - Number of hours that should be considered for each typical period, i.e. day in the aggregated timeseries
  - __TBD__

`plot()`
- __Not implemented yet!!__

`errors()`
- Return input error in the form of RMSE and MAE using `tsam.accuracyIndicatiors()`

`minErros()`
- Find and print cluster method that yields the minimal error of all given method specified by `_clusterMethods`