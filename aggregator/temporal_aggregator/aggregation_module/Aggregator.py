"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

import copy
import os.path
import sys
import matplotlib.pyplot as plt
import pandas as pd
from pandas.core.frame import DataFrame
import tsam.timeseriesaggregation as tsam
from tqdm import tqdm

class Aggregator:
    # Constructor
    def __init__(self, _input, clusterMethods=['k_means']):

        self.__indices = []
        self.__clusterMethods = clusterMethods
        self.__input = _input

        self.__supportedClusterMethods = ["averaging", "k_means", "k_medoids", "hierarchical"]

        if isinstance(self.__input, str):
            # input given as file path --> reading required
            print("Input is String/Path")
            try:
                self.__rawTimeSeries = self.__readTS(self.__input)
            except FileNotFoundError:
                print("ERROR: ", self.__input, " does not exist or is not a file. Aborting...")
                return
            except Exception as e:
                print("ERROR: Unexpected error when trying to read file: ", self.__input)
                print(e)
                print("Aborting...")
                return
        elif isinstance(self.__input, pd.DataFrame): # FIXME: check instance of time series
            print("Input is DataFrame")
            self.__rawTimeSeries = self.__input
        elif isinstance(self.__input, dict):
            # We expect input_profiles(dict) was directly passed
            print("Input is a Dict")
            self.__rawTimeSeries, self.__keys = self.__preprocessDict(self.__input)
        else:
            print("ERROR: ", type(self.__input), " is no valid input type. Aborting...")
            return


        self.__rawTimeSeries = self.__sanitizeColNames(self.__rawTimeSeries)
        self.__indices = self.__rawTimeSeries.columns
        print("Identified the following column names: " + str(self.__indices.values))

        self.__aggregations = {}
        self.__aggregations["raw"] = self.__rawTimeSeries
        self.__errors = {}
        self.__errorsCalculated = False
        self.__hoursPerPeriod = 24

    #####################################################################
    # Public:
    def setClusterMethods(self, clusterMethods):
        print("Warning: Changing cluster methods results in deprecated aggregations and errors!!")
        print("It is recommended to call aggregate() (again)...")
        self.__clusterMethods = clusterMethods

    def getClusterMethods(self):
        return self.__clusterMethods

    def getClusterMethodsAll(self):
        return self.__supportedClusterMethods

    def getTS(self):
        return self.__rawTimeSeries

    def getInputKeys(self):
        return self.__keys

    def getAggregations(self):
        return self.__aggregations

    # return aggregated time series as dict to use directly in prosumer framework
    # Error occurs if hoursPerPeriod/24 has more than 2 no of digits after the decimal point
    def getAggregationsDict(self):
        output = {}
        step = 24/self.__hoursPerPeriod
        for clusterMethod in self.__clusterMethods:
            tmp = {}
            # for col, key in enumerate(self.__keys):
            for key in self.__keys:
                # tmp[key] = self.__aggregations[clusterMethod]['typ_periods'].iloc[:,col]
                tmp[key] = self.__aggregations[clusterMethod]['typ_periods'][key]
                #FIXME: Index shouldnt be start at hardcoded time!
                tmp[key].index = pd.date_range(start='1/1/2020', periods=tmp[key].size, freq=str(step)+'H')
            output[clusterMethod] = tmp

        return output

    # write timer series to path
    def writeTS(self, path="aggregated_output.csv"):
        for method in self.__clusterMethods:
            self.__aggregations[method]["typ_periods"].to_csv('_'.join([method, path]), index=False)

    # aggregate times eries
    # Beware: when printing the AggregationsDict not all hoursPerPeriod are permissible (see comment for getAggregationsDict())
    def aggregate(self, _noTypicalPeriods = 8, _hoursPerPeriod = 24, _noSegments = 10, \
            _extremePeriodMethod = "None",  _peakMin=[], _peakMax=[], _meanMin=[], _meanMax=[]):
        print("Aggregating time series...")
        ts = self.__removeStringCols(self.__aggregations["raw"])
        for method in tqdm(self.__clusterMethods):
            self.__aggregations[method] = {}
            self.__aggregations[method]["agg_object"] = tsam.TimeSeriesAggregation(ts, \
                noTypicalPeriods=_noTypicalPeriods, hoursPerPeriod=_hoursPerPeriod, noSegments=_noSegments, clusterMethod=method, \
                    extremePeriodMethod=_extremePeriodMethod,\
                    addPeakMin=_peakMin, addPeakMax=_peakMax, addMeanMin=_meanMin, addMeanMax=_meanMax)
            self.__aggregations[method]["typ_periods"] = self.__aggregations[method]["agg_object"].createTypicalPeriods()

        # After a new aggregation, errors must be calculated again
        self.__errorsCalculated = False
        self.__hoursPerPeriod = _hoursPerPeriod

    # plot
    def plot(self, args):
        pass

    # return dict of all estimated errors
    def getErrors(self):
        if not self.__errorsCalculated:
            self.__estimateError()

        ret = {}
        for method in self.__clusterMethods:
            ret[method] = self.__aggregations[method]["error"]

        return ret

    # print all estimated errors
    def printErrors(self):
        if not self.__errorsCalculated:
            self.__estimateError()

        for method in self.__clusterMethods:
            print("Method: ", method, "\n")
            print(self.__aggregations[method]["error"])

    # find and print minmal errors
    def minErrors(self):
        if not self.__errorsCalculated:
            self.__estimateError()

        for index in self.__indices:
            df = self.__errors[index].T.min().to_frame(name="error")
            df.insert(1, "method", self.__errors[index].T.idxmin().to_frame())
            print("##################################")
            print(index, ".min = \n", df, "\n")
            print("##################################")


    #####################################################################
    # Private:

    # read time series at path
    def __readTS(self, path, _start = '1/1/2018', _freq='T'):
        # FIXME: Hardcoded variables for index not flexible!
        ts = pd.read_csv(path)
        date_index = pd.date_range(start=_start, periods=ts.shape[0], freq=_freq)
        ts.set_index([date_index], inplace=True)
        return ts

    # preprocess dictionary input and create single DatFrame
    def __preprocessDict(self, input_dict):
        keys = input_dict.keys()
        print(keys)
        df = pd.concat([input_dict[key].rename(key) for key in input_dict.keys()], axis=1)
        return df, keys

    # plot timeseries
    # TODO: Currently taken from tsam examples --> rework for our usecase
    def __plotTS(self, data, title, periodlength, vmin, vmax):
        fig, axes = plt.subplots(figsize = [6, 2], dpi = 100, nrows = 1, ncols = 1)
        stacked, timeindex = tsam.unstackToPeriods(copy.deepcopy(data), periodlength)
        cax = axes.imshow(stacked.values.T, interpolation = 'nearest', vmin = vmin, vmax = vmax)
        axes.set_aspect('auto')
        axes.set_ylabel('Hour')
        plt.xlabel('Day')
        fig.canvas.set_window_title(title)

        fig.subplots_adjust(right = 1.2)
        cbar=plt.colorbar(cax)
        cbar.set_label('Diffuse')

    # predict original data from typical periods, i.e. aggregated data
    def __predictData(self):
        for method in self.__clusterMethods:
            self.__aggregations[method]["predicted_data"] = \
                self.__aggregations[method]["agg_object"].predictOriginalData()

    # estimate aggregation error
    def __estimateError(self):
        print("Compute aggregation error:")
        for i, method in tqdm(enumerate(self.__clusterMethods)):
            self.__aggregations[method]["error"] = \
                    self.__aggregations[method]["agg_object"].accuracyIndicators().transpose()
            for col in self.__aggregations[method]["error"].columns:
                if i == 0: self.__errors[col] = pd.DataFrame()
                self.__errors[col][method] = self.__aggregations[method]["error"][col]

        self.__errorsCalculated = True

    # remove string columns as they cannot be aggregated
    def __removeStringCols(self, ts):
        timeseries = ts
        for index in self.__indices:
            if isinstance(timeseries[index][0],str):
                timeseries = timeseries.drop(columns=[index])
                print("Column '" + index + "' has type String and is dropped..." )
        return timeseries

    def __sanitizeColNames(self, df):
        df.columns = df.columns.astype(str)
        if not df.columns.is_unique:
            newIndex = []
            seenIndices = set()
            for index in df.columns:
                if index in seenIndices:
                    newIndex.append("{}_{}".format(index, 1))
                else:
                    newIndex.append(index)
                seenIndices.add(newIndex[-1])
            df.columns = newIndex
        return df

    # estimate local variances
    def __estimateVar(self):
        pass
