"""
MIT License

Copyright (c) 2023 RWTH Aachen University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import pytest
import pickle
import pandas as pd
from temporal_aggregator import aggregation_module as am
import re
import os


PATH = os.path.join(os.path.dirname(__file__), "test_inputs")
PATH_VERIFY = os.path.join(os.path.dirname(__file__), "verifications")

print(PATH)
print(PATH_VERIFY)

#Fixture which creates an instance of the aggregator for the class
#Note: There is one instance for all tests using this fixture -> reset instance between tests
@pytest.fixture(scope="class")
def agg():
    agg = am.Aggregator(os.path.join(PATH, "input_time_series.csv"), clusterMethods=['hierarchical'])
    agg.aggregate()
    return agg


#Class to test the Aggregator Module
@pytest.mark.Aggregator
class TestClass_Aggregator:

    ########## init_test #########
    @pytest.mark.init_func
    def test_init(self):
        agg = am.Aggregator(os.path.join(PATH,"input_time_series.csv"))
        assert isinstance(agg, am.Aggregator)

    @pytest.mark.init_func
    def test_dict_input(self):
        input_profiles_input = pickle.load(open(os.path.join(PATH, "input_dict.pkl"), "rb"))
        agg = am.Aggregator(input_profiles_input)
        assert isinstance(agg, am.Aggregator)

    ######### setter ##########
    @pytest.mark.setter
    def test_setClusterMethod(self):
        #create local aggregator for this test
        agg = am.Aggregator(os.path.join(PATH,"input_time_series.csv"), clusterMethods=['hierarchical'])
        agg.aggregate()
        #change cluster method
        agg.setClusterMethods(['averaging'])
        agg.aggregate()     #aggregate after setting cluster method
        methods = agg.getClusterMethods()
        assert methods == ['averaging']


    ######### functionality ##########
    @pytest.mark.functionality
    def test_aggregate(self, agg):
        agg.aggregate()
        assert True

    ######### getter ###########
    @pytest.mark.getter
    def test_getErrors(self, agg):
        errors = agg.getErrors()
        df = pd.DataFrame({'beam': [0.15486908784163522,0.017474605731383628,0.06286260042398298], 'diffuse': [0.0826727986429539,0.008731950193437457,0.043786317711784074], 'reflected': [0.08428448707115882,0.014155687847503321,0.041584627892177164]},index=['RMSE', 'RMSE_duration', 'MAE'])
        assert errors['hierarchical'].equals(df)

    @pytest.mark.getter
    def test_getClusterMethod(self, agg):
        assert agg.getClusterMethods() == ['hierarchical']

    @pytest.mark.getter
    def test_getClusterMethodAll(self, agg):
        assert agg.getClusterMethodsAll() == ["averaging", "k_means", "k_medoids", "hierarchical"]

    @pytest.mark.getter
    def test_getTS(self, agg):
        raw = agg.getTS()
        verify = pd.read_pickle(os.path.join(PATH_VERIFY,"verify_TS.pkl"))      #verification is saved as pickle-file
        assert raw.equals(verify)

    @pytest.mark.getter
    def test_getAggregations(self, agg):
        data = agg.getAggregations()
        with open(os.path.join(PATH_VERIFY, 'verify_Aggregations.pkl'), 'rb') as handle:  #verification is saved as pickle-file
            verify = pickle.load(handle)
        assert data.keys() == verify.keys()
        assert data['raw'].equals(verify['raw'])
        assert data['hierarchical']['typ_periods'].equals(verify['hierarchical']['typ_periods'])
        assert data['hierarchical']['error'].equals(verify['hierarchical']['error'])


    ########## print funcs ##########
    @pytest.mark.output
    def test_printErrors(self, capsys, agg):
        agg.getErrors()                 #ensure that errors are calculated before printing
        captured = capsys.readouterr()  #dump any output this calculation has caused
        agg.printErrors()
        captured = capsys.readouterr()  #get captured output
        captured = re.findall(r'-?\d+\.?\d*', str(captured))    #find the numbers in the string
        assert captured == ['0.154869', '0.082673', '0.084284', '0.017475', '0.008732', '0.014156', '0.062863', '0.043786', '0.041585']

    @pytest.mark.output
    def test_minErrors(self, capsys, agg):
        agg.getErrors()                 #ensure that errors are calculated before printing
        captured = capsys.readouterr()  #dump any output this calculation has caused
        agg.minErrors()
        captured = capsys.readouterr()  #get captured output
        captured = re.findall(r'-?\d+\.?\d*', str(captured))    #find the numbers in the string
        assert captured == ['0.082673', '0.008732', '0.043786', '0.084284', '0.014156', '0.041585', '0.154869', '0.017475', '0.062863']

    @pytest.mark.output
    def test_writeTS(self, agg):
        agg.writeTS("agg_output.csv")
        assert os.path.isfile("hierarchical_agg_output.csv")
        os.remove("hierarchical_agg_output.csv") #only executes when assertion true